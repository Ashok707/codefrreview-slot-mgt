﻿
//Required headers

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unite.Domain.Core.Entities;
using Unite.Domain.Core.Services;
using Unite.Web.Api.ActionFilters;
using Newtonsoft.Json;
using Unite.Web.ActionFilters;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using System.Web;
using System.Net.Http.Headers;
using System.Data;
using System.ComponentModel;
using Unite.Reports;
using Unite.Infrastructure.Sql;
using Unite.Domain.Core.Repositories;
using Unite.Infrastructure.Services;
using Unite.Infrastructure.SQL.Repositories;


namespace Unite.Web.Api.Controllers
{
    //Listing all the Visits

    [RoutePrefix("api/visit")]
    public class VisitController : CustomApiController
    {
        private readonly IVisitServices _visitServices;

        //Doctor visting hours in the clinics are listed using the following function
        public VisitController(IVisitServices visitServices)
        {
            _visitServices = visitServices;
        }

       //storing the Insurance Card Images
        [HttpGet, Route("InsuranceCardImage")]
        public IList<PatientImageInfo> GetPatientInsuranceCardImage(long patientInsuranceId, long patientPin, long clinicId, long userId)
        {
            return _visitServices.GetPatientInsuranceCardImage(patientInsuranceId, patientPin, clinicId, userId);
        }

        //Listing the patients in queue based on the appoinments
        [HttpGet, Route("queue")]
        public PatientVisitQueueAndTotal GetPatientInQueue(long clinicId, long userId, string callingFrom, bool isAllDoctor, long patientPin, Nullable<DateTime> visitDate, Nullable<DateTime> visittodate)
        {
            return _visitServices.GetPatientQueue(clinicId, userId, callingFrom, isAllDoctor, patientPin, visitDate, visittodate);
        }

        //Getting the Appointment as per the doctor
        [HttpGet, Route("Appointment_Schedule")]
        public IList<Appointment_Schedule> GetDoctorAppointmentsSchedule(long doctorId, DateTime appointmentDate, string appointmentStatus, long specialityId, long clinicId, string appointmentType)
        {
            return _visitServices.GetDoctorAppointmentsSchedule(doctorId, appointmentDate, appointmentStatus, specialityId, clinicId, appointmentType);
        }

        //Regering the appointments based on the clinic & doctor id
        [HttpPost, HttpGet, Route("verifyevent")]
        public HttpResponseMessage VerifyEvent(long doctorId, long clinicId, string duration, DateTime appointmentDate, DateTime appointmentTime)
        {
            string message;
            message= _visitServices.VerifyEvent(doctorId, clinicId, duration, appointmentDate, appointmentTime);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, message);
            response.Content = new StringContent("{\"Message\": \"" + message + "\"}", System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        //pre booking the appointments if the slots are empty
        [HttpGet, Route("precheck/{visitId}")]
        public IList<Precheck> GetPrecheckByVisitId(long visitId)
        {
            return _visitServices.GetPrecheckByVisitId(visitId);
        }

        //Listing the details of the visited Patients
        [HttpGet, Route("patientVisitDetails/{userId}/{visitId}/{clinicId}/{departmentId}")]
        public PatientVisitRecords GetPatientVisitDetailByVisitId(long userId, long visitId, long clinicId, long departmentId)
        {
            return _visitServices.GetPatientVisitDetailByVisitId(userId, visitId, clinicId, departmentId);
        }

        //Based on the visit id listing the patient information
        [HttpGet, Route("patientvisit/{visitId}")]
        public IList<PatientVisit> GetPatientDetailByVisitId(long visitId)
        {
            return _visitServices.GetPatientDetailByVisitId(visitId);
        }

        //Updating the informations on the appointments
        [HttpPost, HttpGet, Route("addUpdateEvent")]
        public HttpResponseMessage AddUpdateEvent(long userId, long doctorId, long clinicId, long specialityId, string preAuth, string pin, string firstName, string mobileNo, string requestedby, string notes, string sms, string email, bool isFollowup, string duration, DateTime appointmentTime, bool isSms, bool isEmail, string appointmentType, long appointmentId, long departmentId)
        {
            bool saved = false;
           long newAppointmentId = _visitServices.AddUpdateEvent(userId, doctorId, clinicId, specialityId, preAuth, pin, firstName, mobileNo, requestedby, notes, sms, email, isFollowup, duration, appointmentTime, isSms, isEmail, appointmentType, appointmentId, departmentId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            response.Content = new StringContent("{\"Message\": \"" + newAppointmentId + "\"}", System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        //Scheduling the informations on the appointments with filters
        [HttpPost, HttpGet, Route("schedule")]
        public Scheduler GetAppointmentsWithFilter(long clinicId, int day, int month, int year, char option, long doctorId, char cdtag, DateTime from, DateTime to, long departmentId, long userId, string isCancelVisit = "")
        {
            return _visitServices.GetAppointmentsWithFilter(clinicId, day, month, year, option, doctorId, cdtag, from, to, departmentId, userId, isCancelVisit);
        }

        //Payment information of the patients
        [HttpPost, HttpGet, Route("PendingRemittance")]
        public IList<PendingRemittance> GetPendingRemittance(long patientPin, long clinicId, long userId)
        {
            return _visitServices.GetPendingRemittance(patientPin, clinicId, userId);
        }


        //Giving color code on the Scheduled slots or appointments
        [HttpGet, Route("ScheduleIntervalColor/{clinicId}/{userId}")]
        public IList<ScheduleIntervalColor> GetScheduleIntervalColor(long clinicId, long userId)
        {
            return _visitServices.GetScheduleIntervalColor(clinicId, userId);
        }

        //Getting the menu data as per the status of the appointments
        [HttpPost, HttpGet, Route("ContextMenuAppointmentStatus")]
        public IList<ContextMenuAppointmentStatus> GetContextMenuAppointmentStatus(string status, long clinicId, long userId)
        {
            return _visitServices.GetContextMenuAppointmentStatus(status, clinicId, userId);
        }

        //updating the menu data as per the status of the appointments
        [HttpPost, HttpGet, Route("UpdateAppointmentStatus")]
        public string UpdateAppointmentStatus(long appointmentId, string status, long userId, string remarks, long clinicId, long departmentId)
        {
            return _visitServices.UpdateAppointmentStatus(appointmentId, status, userId, remarks, clinicId, departmentId);

        }

        //getting the header information of the patient visit page
        [HttpPost, HttpGet, Route("PatientVisitHeaderAdd")]
        public HttpResponseMessage PatientVisitHeaderAdd(long appointmentId, long userId, long patientPin = 0,long visitId = 0)
        {
            bool saved = false;
            _visitServices.PatientVisitHeaderAdd(appointmentId, userId, patientPin, visitId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            return response;
        }

        //Rescheduling the Appointments
        [HttpPost, HttpGet, Route("RescheduleAppointment")]
        public HttpResponseMessage RescheduleAppointment(long appointmentId, long userId, long doctorId, long specialityId, string duration, DateTime appointmentDate, string remarks, DateTime time, string status, long clinicId, long departmentId, bool isSendSms)
        {
            bool saved = false;
            _visitServices.RescheduleAppointment(appointmentId, userId, doctorId, specialityId, duration, appointmentDate, remarks, time, status, clinicId, departmentId, isSendSms);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            return response;
        }

        //Patient visit history information
        [HttpGet, Route("visithistory")]
        public IList<VisitHistory> GetVisitHistory(string pin, long clinicId, long userId)
        {
            return _visitServices.GetVisitHistory(pin, clinicId, userId);

        }

        //Patient book appointments on the post dates
        [HttpGet, Route("VisitHistoryFuture")]
        public IList<VisitHistoryFuture> GetVisitHistoryFuture(string pin, long clinicId, long userId)
        {
            return _visitServices.GetVisitHistoryFuture(pin, clinicId, userId);
        }

        //Listing the Appointment Duration
        [HttpGet, Route("AppointmentDuration")]
        public IList<AppointmentDurationByDoctor> GetAppointmentDurationByDoctor(string doctorId)
        {
            return _visitServices.GetAppointmentDurationByDoctor(doctorId);
        }

        //Listing all the Department
        [HttpPost, HttpGet, Route("Department")]
        public IList<Department> GetAllDepartment(long clinicId)
        {
            return _visitServices.GetAllDepartment(clinicId);
        }

        //Listing the consultation information on the patients
        [HttpGet, Route("coplist/{paramParentId}/{doctorId}/{userId}/{clinicId}/{searchData}/{CasesheetHierarchyId}")]
        [CompressFilter]
        public IList<ConsultationParams> GetConsultationInfo(long paramParentId, long doctorId, long userId, long clinicId, string searchData, long CasesheetHierarchyId)
        {
            return _visitServices.GetConsultationInfo(paramParentId, doctorId, userId, clinicId, searchData, CasesheetHierarchyId);
        }

        //Listing the consulation info clinic wise it also includes cost data
        [HttpGet, Route("CopListAndTabCollection/{visitId}/{userId}/{clinicId}")]
        [CompressFilter]
        public CopListTabCollection GetConsultationInfoTabInfo(long visitId, long userId, long clinicId, string priceDate)
        {
            return _visitServices.GetConsultationInfoTabInfo(visitId, userId, clinicId, priceDate);
        }


        //Search based on consultation  with parameters
        [HttpGet, Route("ConsultationParamSearchList")]
        [CompressFilter]
        public IList<DiagnosisInfo> GetConsultationSearchInfo(long VisitId, long ConsultationParentId, string searchString, long UserId, long ClinicId, string priceDate)
        {
            return _visitServices.GetConsultationSearchInfo(VisitId, ConsultationParentId, searchString, UserId, ClinicId, priceDate);
        }

        //storing the remarks data
        [HttpPost, Route("coplist/save/{clinicId}/{userId}")]
        public HttpResponseMessage SaveRemarks(IList<DiagnosisInfo> remarks, long clinicId, long userId)
        {
            bool saved = false;
            saved = _visitServices.SaveConsultationInfo(remarks, clinicId, userId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            return response;
        }

        //storing the precheck information data
        [HttpPost, Route("precheck/save")]
        public HttpResponseMessage SavePrecheck(IList<Precheck> precheck)
        {
            bool saved = false;
            saved = _visitServices.SavePrecheck(precheck);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        //Storing the Pregnant info data
        [HttpPost, Route("isPregnant/save")]
        public HttpResponseMessage UpdateIsPregnant(IList<IsPregnant> isPregnant)
        {
            bool saved = false;
            saved = _visitServices.UpdateIsPregnant(isPregnant);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }


        //getting the consultation Notes from the earlier visits
        [HttpGet, Route("consultation/getNotes")]
        public ConsultationHeader GetConsultationHeaderByVisitId(long visitId)
        {
            return _visitServices.GetConsultationHeaderByVisitId(visitId);
        }

        //storing the current consultation data
        [HttpPost, Route("consultaiton/savenotes")]
        public HttpResponseMessage UpdateConsultationHeader(ConsultationHeader consultationHeader)
        {
            bool saved = false;
            saved = _visitServices.UpdateConsultationHeader(consultationHeader);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            return response;
        }


        //saving the patient visit data
        [HttpPost, Route("patientvisit/save")]
        public HttpResponseMessage SavePatientDetail(IList<PatientVisit> patientVisit)
        {
            bool saved = false;
            saved = _visitServices.SavePatientDetail(patientVisit);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            return response;
        }

        //saving the presriped data from the doctor to the patient
        [HttpPost, Route("Medication/save/{clinicId}")]
        public HttpResponseMessage SavePrescriptionDetail(IList<Prescription> prescriptionData, long clinicId)
        {
            bool saved = false;
            saved = _visitServices.SavePrescriptionDetail(prescriptionData, clinicId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            return response;
        }

        //getting the blocked datas from the master tables
        [HttpGet, Route("getLockMaster")]
        public IList<LockMaster> GetLockMasterData()
        {
            return _visitServices.GetLockMasterData();
        }

        //getting the blocked datas from the master tables with patient pin
        [HttpGet, Route("getLockMaster/{patientPin}")]
        public IList<LockMaster> GetLockMasterData(long patientPin = 0)
        {
            return _visitServices.GetLockMasterData(patientPin);
        }

        //getting the blocked datas from the master tables along with block master id
        [HttpGet, Route("getLockMaster/{patientPin}/{lockMasterId}")]
        public IList<LockMaster> GetLockMasterDataByLockId(long patientPin = 0, long lockMasterId = 0)
        {
            return _visitServices.GetLockMasterDataByLockId(patientPin, lockMasterId);
        }

        //storing the values to the lock master table
        [HttpPost, Route("InsertLockMaster/{clinicId}/{userId}")]
        public HttpResponseMessage InsertLockMaster(LockMaster lockMaster, long clinicId, long userId)
        {
            long lockId;
            lockId = _visitServices.InsertLockMaster(lockMaster, clinicId, userId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, lockId);
            response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");

            return response;
        }


        //updating the values to the lock master table
        [HttpPost, Route("UpdateLockMaster")]
        public HttpResponseMessage UpdateLockMaster(LockMaster lockMaster)
        {
            bool lockId;
            lockId = _visitServices.UpdateLockMaster(lockMaster);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, lockId);
            response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");

            return response;
        }

        //Releasing the locked or blocked data
        [HttpPost, Route("ReleaseAndAcquireLockMaster/{patientPin}/{lockMasterId}/{visitId}/{userId}")]
        public HttpResponseMessage ReleaseAndAcquireLockMaster(long patientPin, long lockMasterId, long visitId, long userId)
        {
            bool lockId;
            lockId = _visitServices.ReleaseAndAcquireLockMaster(patientPin, lockMasterId, visitId, userId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, lockId);
            return response;
        }

        //searchign the LocHistorBy LockId
        [HttpGet, Route("getLockHistoryByLockId/{lockId}")]
        public IList<LockHistory> GetLockHistoryByLockId(long lockId = 0)
        {
            return _visitServices.GetLockHistoryByLockId(lockId);
        }


        //getting the Allergytypes values
        [HttpGet, Route("Allergy/GetAllergyType")]
        public IList<AllergyType> GetAllergyType()
        {
            return _visitServices.GetAllergyType();
        }

        //creating New Patient Allergies based on the observation of the patients
        [HttpPost, Route("Allergy/SavePatientAllergies/{clinicId}")]
        public HttpResponseMessage UpdatePatientAllergies(IList<PatientAllergies> patientAllergies, long clinicId)
        {
            bool saved = false;
            saved = _visitServices.UpdatePatientAllergies(patientAllergies, clinicId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        //Listing all the Patient Allergies
        [HttpGet, Route("Allergy/GetPatientAllergies/{patientPin}")]
        public IList<PatientAllergies> GetPatientAllergies(long patientPin)
        {
            return _visitServices.GetPatientAllergies(patientPin);
        }

        //Listing all the Patient Allergies Pin,clinic & by patients ids
        [HttpGet, Route("Allergy/GetPatientAllergiesByPamPin/{patientPin}/{clinicId}/{userId}")]
        public IList<PatientAllergies> GetPatientAllergiesForQueueByPamPin(long patientPin, long clinicId, long userId)
        {
            return _visitServices.GetPatientAllergiesForQueueByPamPin(patientPin, clinicId, userId);
        }

        //Updating the common allergies as per the clinic
        [HttpPost, Route("Allergy/SaveAllergyTypes/{clinicId}")]
        public HttpResponseMessage UpdateAllergyType(AllergyType allergyType, long clinicId)
        {
            bool saved = false;
            saved = _visitServices.updateAllergyType(allergyType, clinicId);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }


        //Getting the patient history 
        [HttpGet, Route("getPreCheckHistoryByPatientPin/{patientPin}")]
        public DataTable GetPreCheckHistoryByPatientPin(long patientPin)
        {
            return _visitServices.GetPreCheckHistoryByPatientPin(patientPin);
        }

        //getting the patient details
        [HttpGet, Route("GetAllPatientDetails")]
        public IList<PatientList> GetAllPatientDetails()
        {
            return _visitServices.GetAllPatientDetails();
        }

        //getting the patient details by patient pin
        [HttpGet, Route("GetPatientDetailsByPatientPin")]
        public IList<PatientList> GetPatientDetailsByPatientPin(long patientPin, string firstName, string mobileNo, long clinicId, long userId)
        {
            if (firstName == null)
            {
                firstName = "";
            }
            if (mobileNo == null)
            {
                mobileNo = "";
            }
            return _visitServices.GetPatientDetailsByPatientPin(patientPin, firstName, mobileNo, clinicId, userId);
        }

        //getting the patient visit on early dates
        [HttpGet, HttpPost, Route("CheckVisitAlreadyExist")]
        public HttpResponseMessage CheckVisitAlreadyExist(long patientPin, long doctorId, DateTime checkinDate, long departmentId, long userId, long clinicId, long loginUserId)
        {
            Boolean ResponseMesg = _visitServices.CheckVisitAlreadyExist(patientPin, doctorId, checkinDate, departmentId, userId, clinicId, loginUserId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent("{\"Message\": \"" + ResponseMesg + "\"}", System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        //Saving the Payment Details
        [HttpPost, Route("BillingHead/save/{clinicId}")]
        public HttpResponseMessage SaveBillingHeadDetail(IList<BillingHeads> billingHeadData, long clinicId)
        {
            bool saved = false;
            saved = _visitServices.SaveBillingHeadDetail(billingHeadData, clinicId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            return response;
        }

        //saving all the information about the patient visit
        [HttpPost, Route("SaveVisitDetails/{clinicId}/{userId}/{isForceSave}")]
        public IList<DHAValidationData> SaveVisitDetails(Newtonsoft.Json.Linq.JObject data, long clinicId, long userId, Boolean isForceSave)
        {
            try
            {
                IList<PatientVisit> patientVisit = JsonConvert.DeserializeObject<List<PatientVisit>>(data["Patientvisit"].ToString());
                IList<Precheck> precheck = JsonConvert.DeserializeObject<List<Precheck>>(data["PreChecks"].ToString());
                IList<DiagnosisInfo> ConsultationInfo = JsonConvert.DeserializeObject<List<DiagnosisInfo>>(data["CondetParamsList"].ToString());
                ConsultationHeader consultationHeader = JsonConvert.DeserializeObject<ConsultationHeader>(data["Consultation"].ToString());
                IList<Prescription> PrescriptionDataList = JsonConvert.DeserializeObject<List<Prescription>>(data["PrescriptionData"].ToString());
                IList<BillingHeads> BillingHead = JsonConvert.DeserializeObject<List<BillingHeads>>(data["BillingHead"].ToString());
                PatientCasesheetHdr patientCaseSheetHeader = JsonConvert.DeserializeObject<PatientCasesheetHdr>(data["PatientCasesheetHdr"].ToString());
                IList<PatientCasesheetDet> patientCaseSheetDetail = JsonConvert.DeserializeObject<List<PatientCasesheetDet>>(data["PatientCasesheetDet"].ToString());
                IList<PatientCasesheetList> patientCaseSheetList = JsonConvert.DeserializeObject<List<PatientCasesheetList>>(data["PatientCasesheetList"].ToString());
                IList<GlassPrescription> glassPrescriptionList = JsonConvert.DeserializeObject<List<GlassPrescription>>(data["GlassPrescriptionData"].ToString());
                Boolean DHAValidationData = _visitServices.GetDHAValidationAvailable(userId, clinicId);
                IList<DHAValidationData> _DHAValidationData;
                if (DHAValidationData == true && isForceSave == false && patientVisit[0].PaymentType == "I")
                {
                    _DHAValidationData = CheckDHAValidation(precheck, ConsultationInfo, userId, clinicId, Convert.ToInt64(patientVisit[0].VisitId));
                    if (_DHAValidationData.Count > 0)
                    {
                        return _DHAValidationData;
                    }
                }
                else
                {
                    _DHAValidationData = new List<DHAValidationData>();
                }
                if (_DHAValidationData.Count == 0)
                {
                    SavePatientDetail(patientVisit);
                    SavePrecheck(precheck);
                    SaveRemarks(ConsultationInfo, clinicId, userId);
                    UpdateConsultationHeader(consultationHeader);
                    SavePrescriptionDetail(PrescriptionDataList, clinicId);
                    SaveBillingHeadDetail(BillingHead, clinicId);
                    SavePatientCasesheetDetails(patientCaseSheetHeader, patientCaseSheetDetail, patientCaseSheetList, clinicId);
                    SaveGlassPrescriptionDetails(glassPrescriptionList);
                    UpdateReportStatistics("MEDICALRECORDS", Convert.ToInt64(patientVisit[0].VisitId.ToString()), userId);
                }
                return _DHAValidationData;
            }
            catch (Exception)
            {
                throw;
            }
        }


        //Listing the report statistics
        [HttpGet, Route("UpdateReportStatistics")]
        public HttpResponseMessage UpdateReportStatistics(string moduleName, long patientVisitId, long userId)
        {
            bool saved = false;
            saved = _visitServices.UpdateReportStatistics(moduleName, patientVisitId, userId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        // Listing the DHA validation information
        public IList<DHAValidationData> CheckDHAValidation(IList<Precheck> preCheck, IList<DiagnosisInfo> consultationInfo, long userId, long clinicId, long visitId)
        {
            IList<DHAValidationData> _DHAValidationData;
            try
            {
                _DHAValidationData = _visitServices.DHAValidationCheck(preCheck, consultationInfo, userId, clinicId, visitId);
                return _DHAValidationData;
            }

            catch (Exception)
            {
                throw;
            }
        }

        //Saving the prescription details
        public HttpResponseMessage SaveGlassPrescriptionDetails(IList<GlassPrescription> glassPrescriptionList)
        {
            bool saved = false;
            saved = _visitServices.SaveGlassPrescriptionDetails(glassPrescriptionList);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            return response;
        }

        //Listing all the patients information in a queue
        [HttpGet, Route("VisitQueue")]
        public VisitQueueAndTotal GetAllPatientInQueue(DateTime appointmentDateTime, long userId, long clinicId)
        {
            return _visitServices.GetAllPatientInQueue(appointmentDateTime, userId, clinicId);
        }

        //Listing the Doctors based on the Speciality
        [HttpGet, Route("SpecialityByDoctor")]
        public IList<DoctorSpeciality> GetSpecialitybyDoctor(long doctorId)
        {
            return _visitServices.GetSpecialitybyDoctor(doctorId);
        }

        //Listing all  the Canceled Appointments
        [HttpPost, HttpGet, Route("VisitQueue/Cancel")]
        public HttpResponseMessage CancelAppointment(long appointmentId, long visitId, string remarks)
        {
            bool saved = false;
            saved = _visitServices.CancelAppointment(appointmentId, visitId, remarks);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }

            return response;
        }

        //Updating the Doctor Availability
        [HttpPost, Route("VisitQueue/ChangeDcotor/{appointmentId}/{visitId}/{doctorId}/{SPM_ID}/{userId}/{departmentId}")]
        public HttpResponseMessage ChangeDoctor(long appointmentId, long visitId, long doctorId, long specialityId, long userId, long departmentId)
        {
            bool saved = false;
            saved = _visitServices.ChangeDoctor(appointmentId, visitId, doctorId, specialityId, userId, departmentId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            return response;
        }

        //Listing the Patient Insurance based on Pin
        [HttpGet, Route("PatientInsurancebyPIN/{patientPin}")]
        public IList<PatientInsuranceDetails> GetPatientInsuranceDetailsbyPatientPin(long patientPin)
        {
            return _visitServices.GetPatientInsuranceDetailsbyPatientPin(patientPin);
        }

        //saving the Referral Details from other clinics
        [HttpPost, Route("ReferralBy/saveItems/{clinicId}")]
        public HttpResponseMessage InsertReferralMaster(ReferralMaster referralMaster, long clinicId)
        {
            string ItemCode;
            ItemCode = _visitServices.InsertReferralMaster(referralMaster, clinicId);
            string responseJson = "{\"Response\":\"" + ItemCode + "\"}";
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(responseJson, System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        //saving the Patient informations Based on Referrals
        [HttpPost, Route("PatientVisit/UpdateReferral/{referralMasterId}/{visitId}/{isInternalDoctor}/{userId}")]
        public HttpResponseMessage UpdatePatientVisitHeaderReferral(long referralMasterId, long visitId, Boolean isInternalDoctor, long userId)
        {
            bool ReferalAdd = false;
            ReferalAdd = _visitServices.UpdatePatientVisitHeaderReferral(referralMasterId, visitId, isInternalDoctor, userId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ReferalAdd)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        //saving the Payment informations who are consulted the doctor  with NoCharges
        [HttpPost, Route("NoChargeCheck/{visitId}/{userId}")]
        public HttpResponseMessage NoChargeCheck(long visitId, long userId)
        {
            string Alert;
            Alert = _visitServices.NoChargeCheck(visitId, userId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent("{\"Response\":\"" + Alert + "\"}", System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        //updating the Payment informations who are consulted the doctor  with NoCharges
        [HttpPost, Route("VisitNoCharge/{visitId}/{userId}")]
        public HttpResponseMessage UpdatePatientVisitHeaderNoCharge(long visitId, long userId)
        {
            bool ReferalAdd = false;
            ReferalAdd = _visitServices.UpdatePatientVisitHeaderNoCharge(visitId, userId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ReferalAdd)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        //saving the Appointment Status tooltip information
        [HttpPost, HttpGet, Route("AppointmentStatus_tooltip")]
        public IList<ContextMenuAppointmentStatus> GetAppointmentStatusTooltip(string status, long clinicId, long userId)
        {
            return _visitServices.GetAppointmentStatusTooltip(status, clinicId, userId);
        }

        //getting the Shrink Details of the patients
        [HttpGet, HttpPost, Route("ShrinkDetailsByUser")]
        public HttpResponseMessage ShrinkDetailsByUser(long userId, int numResources, int shrinkSubInterval, string displayText, string displayGender, long clinicId)
        {
            bool ReferalAdd = false;
            ReferalAdd = _visitServices.ShrinkDetailsByUser(userId, numResources, shrinkSubInterval, displayText, displayGender, clinicId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ReferalAdd);
            return response;
        }

        //Getting the SMS Balances clinic wise
        [HttpGet, Route("SMS_BalanceValidate/{userId}/{clinicId}")]
        public IList<CodeMasterUser> GetSMSBalanceLimitConfiguration(long userId, long clinicId)
        {
            return _visitServices.GetSMSBalanceLimitConfiguration(userId, clinicId);
        }

        //Getting the Queue wise patient details
        [HttpGet, HttpPost, Route("VisitQueue/UpdatePatientDetails")]
        public HttpResponseMessage UpdatePatientDetails(Nullable<DateTime> dob, string mobile, long patientPin, string firstName, string gender, long userId)
        {
            bool UpdatePatient = false;
            UpdatePatient = _visitServices.UpdatePatientDetails(dob, mobile, patientPin, firstName, gender, userId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, UpdatePatient);
            if (UpdatePatient)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        //Saving the  Patient details CaseSheet
        [HttpPost, Route("patientCasesheet/save/{clinicId}")]
        public HttpResponseMessage SavePatientCasesheetDetails(PatientCasesheetHdr patientCaseSheetHeader, IList<PatientCasesheetDet> patientCaseSheetDetail, IList<PatientCasesheetList> patientCaseSheetList, long clinicId)
        {
            bool saved = false;
            saved = _visitServices.SavePatientCasesheetDetails(patientCaseSheetHeader, patientCaseSheetDetail, patientCaseSheetList, clinicId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            return response;
        }

        //gettign the Diabetes ICD Codes
        [HttpGet, Route("getDiabetesICDCodes")]
        public IList<ICDCODE> GetDiabetesICDCodes()
        {
            return _visitServices.GetDiabetesICDCodes();
        }

        //getting the Procedures to list appointments
        [HttpGet, Route("GetProcedures")]
        public IList<ProcedureForScheduler> GetProcedureForAppointment(long userId, long clinicId,string searchString)
        {
            return _visitServices.GetProcedureForAppointment(userId, clinicId, searchString);
        }

        //Saving the Procedures of an Appointment
        [HttpPost, Route("SaveProceduresforAppointment")]
        public HttpResponseMessage SaveProceduresforAppointment(IList<ProcedureForScheduler> ProceduresDataItem)
        {
            bool saved = false;
            saved = _visitServices.SaveProceduresforAppointment(ProceduresDataItem);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        //listing the Schedule Procedure
        [HttpGet, Route("LoadScheduleProcedure/{appointmentId}")]
        public IList<ProcedureForScheduler> LoadAppointmentProcedure(long appointmentId)
        {
            return _visitServices.LoadAppointmentProcedure(appointmentId);
        }

        //Deleting Schedule Procedure based on Appointment Id
        [HttpPost, Route("DeleteScheduleProcedure/{appointmentId}")]
        public HttpResponseMessage DeleteScheduleProcedure(long appointmentId)
        {
            bool saved = false;
            saved = _visitServices.DeleteScheduleProcedure(appointmentId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        //Saving the Patient FollowUp Notes
        [HttpPost, Route("SavePatientFollowUpNotes")]
        public HttpResponseMessage SaveNurseFollowupNotes(Newtonsoft.Json.Linq.JObject data)
        {
            bool saved = false;
            IList<PatientVisitLog> patientVisit = JsonConvert.DeserializeObject<List<PatientVisitLog>>(data["PatientVisitLog"].ToString());
            long clinicId = JsonConvert.DeserializeObject<long>(data["clinicId"].ToString());
            saved = _visitServices.SaveNurseFollowupNotes(patientVisit, clinicId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        //saving the Nurse Notes
        [HttpPost, HttpGet, Route("Save/NurseNotes")]
        public HttpResponseMessage SaveNurseNotes(long visitId, string copInternalDescription, string consultationRemarks, long userId)
        {
            bool saved = false;
            saved = _visitServices.SaveNurseNotes(visitId, copInternalDescription, consultationRemarks, userId);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        //getting the Invoice Details Queue Based on Visit Id
        [HttpGet, Route("InvoiceDetailsQueue/{visitId}")]
        public IList<InvoiceDetailsQueue> GetInvoiceDetailsData(long visitId)
        {
            return _visitServices.GetInvoiceDetailsData(visitId);
        }

        //gettign the Invoice Print Report
        public DataTable GetInvoiceClinicInfo(long clinicId, long userId)
        {
            return _visitServices.GetInvoiceClinicInfo(clinicId, userId);
        }

        //gettign the Invoice detailed data
        public DataTable GetInvoiceDetailsData1(long invoiceInternalNo)
        {
            return _visitServices.GetInvoiceDetailsData1(invoiceInternalNo);
        }
        public DataTable CodeMasterForInvoiceReport(long userId, string key1, string key2, string key3, long visitId, long clinicId, long departmentId)
        {
            return _visitServices.CodeMasterForInvoiceReport(userId, key1, key2, key3, visitId, clinicId, departmentId);
        }
        public DataTable GetClinicCurrency(Int64 clinicId)
        {

            return _visitServices.GetClinicCurrency(clinicId);
        }

        //getting the multiple visits of the patients
        [HttpGet, Route("multipleprecheck/{visitId}")]
        public DataTable GetMultiplePrecheckByVisitId(long visitId)
        {
            return _visitServices.GetMultiplePrecheckByVisitId(visitId);
        }

        //gettign the PreCheck Vitals
        [HttpGet, Route("PreCheckVitals/{visitId}")]
        public DataTable GetPrecheckVitalsByVisitId(long visitId)
        {
            return _visitServices.GetPrecheckVitalsByVisitId(visitId);
        }

        //gettign Price Card information for each consultation
        [HttpGet, Route("PriceCard/DefaultTax/{userId}/{clinicId}/{visitId}")]
        public CodeMasterUser GetDefaultTaxData(long userId, long clinicId, long visitId)
        {
            return _visitServices.GetDefaultTaxData(userId, clinicId, visitId);
        }

        //saving the Lab Work Flow
        [HttpPost, Route("LabWorkFlow/ItemByStatus/{visitId}/{consultationParamId}")]
        public HttpResponseMessage LabWorkFlowGetItemStatus(long visitId, string consultationParamId)
        {
            string responseJson = "{\"Response\":\"" + _visitServices.LabWorkFlowGetItemStatus(visitId, consultationParamId) + "\"}";
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(responseJson, System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        //gettign the Nurse Notes Based on Visit Id
        [HttpGet, Route("NurseNotes/{visitId}/{copInternalDescription}")]
        public HttpResponseMessage SelectNurseNotes(long visitId, string copInternalDescription)
        {
            string responseJson = "{\"Response\":\"" + _visitServices.selectNurseNotes(visitId, copInternalDescription) + "\"}";
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(responseJson, System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        //getting the Header Images of the clinic
        [HttpGet, Route("HeaderImage")]
        public HttpResponseMessage GetHeaderImage(long visitId, long userId)
        {
            string responseJson = "{\"Response\":\"" + _visitServices.getHeaderImage(visitId, userId) + "\"}";
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(responseJson, System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        /getting the Header content of the clinic
        [HttpGet, Route("ProformaHdr")]
        public IList<ProformaHdr> GetProformaHeaderData(long visitId, long userId, long clinicId)
        {
            return _visitServices.getProformaHdrData(visitId, userId, clinicId);
        }

        //Getting the Visit Status of the patients
        [HttpPost, HttpGet, Route("Update/VisitStatus")]
        public HttpResponseMessage UpdatePatientVisitDetails(long visitId, string status, string isPatientPregnancy, long userId, string callFrom, string remarks)
        {
            bool saved = false;
            saved = _visitServices.UpdatePatientVisitDetails(visitId, status, isPatientPregnancy, userId, callFrom, remarks);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saved);
            if (saved)
            {
                response.Content = new StringContent("{\"Message\":\"Success\"}", System.Text.Encoding.UTF8, "application/json");
            }
            else
            {
                response.Content = new StringContent("{\"Message\":\"Failure\"}", System.Text.Encoding.UTF8, "application/json");
            }
            return response;
        }

        //getting & listing the code master information
        [HttpGet, Route("queue/codemaster")]
        public VisitQueueCodeMasterSettings GetPatientQueueCodeMaster(long clinicId, long userId, string callingFrom)
        {
            return _visitServices.GetPatientQueueCodeMaster(clinicId, userId, callingFrom);
        }

        //Getting appointments from the code master 
        [HttpGet, Route("schedule/codemaster")]
        public CodeMasterSettingAppointment GetAppointmentCodeMaster(long clinicId, long userId)
        {
            return _visitServices.GetAppointmentCodeMaster(clinicId, userId);
        }
        [HttpGet, Route("SOAPTab/codemaster")]
        public CodeMasterSettingSOAPTab GetSOAPTabCodeMstr(long VisitId, long clinicId, long userId)
        {
            return _visitServices.GetSOAPTabCodeMstr(VisitId, clinicId, userId);
        }

        //Getting the availability of the doctors in the Department search by clinic wise
        [HttpGet, Route("StatusByDepartment/{userId}/{clinicId}")]
        public IList<CodeMasterUser> GetStatusByDepartments(long userId, long clinicId)
        {
            return _visitServices.GetStatusByDepartments(userId, clinicId);
        }

        //Getting & passsing the values to print the datas in a list based on visit,user & clinic
        [HttpGet, Route("GetLabelPrintData/{VisitId}/{userId}/{clinicId}")]
        public IList<LabelPrintData> GetLabelPrintData(long VisitId, long userId, long clinicId)
        {
            return _visitServices.GetLabelPrintData(VisitId, userId, clinicId);
        }

        //Gettign the  patients information of an Appointment filtererd using clinic id 
        [HttpGet, Route("GetAppointmentByPatietPin/{PatientPin}/{mobileNumber}/{ClinicId}/{UserId}")]
        public IList<CheckAppointment> GetAppointmentBasedOnPamPin(long PatientPin, string mobileNumber, long ClinicId, long UserId)
        {
            return _visitServices.GetAppointmentBasedOnPamPin(PatientPin, mobileNumber, ClinicId, UserId);
        }
    }  
}
