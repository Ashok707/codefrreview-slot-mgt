// All the required headers are listed here
import { Component, OnInit, ViewEncapsulation, ElementRef, HostListener, ViewChild, AfterViewInit, AfterViewChecked, AfterContentInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { SchedulerEvent, Resource, Group, DateChangeEvent, SlotClassArgs } from '@progress/kendo-angular-scheduler';
import { GeneralService } from '../../_services/common/general.service';
import { SchedulerService } from '../../_services/appointment/scheduler.service';
import { QueueStyleClassEnum } from '../../queue/queue-class';
import { BlockUIComponent } from '../../_components/block-ui-component/block-ui-component';
import { IPatientSearchInfo } from '../../_interfaces/patient-master';
import { PatientBannerConfiguration } from '../../_interfaces/patient-visit-queue';
import { IOverDueCollection } from '../../_interfaces/invoice';
import { PatientService } from '../../_services/masters/patient.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DefaultDate, ShortTime, DefaultDateHypen } from '../../_pipes/customDateTime';
import { IvisitqueuePatientRegisterParams } from '../../_interfaces/patient-visit-queue';
import { MenuMasterService } from '../../_services/menuMaster/menuMaster.service';
import { MessageTypeenum, RemarksInput } from '../../_interfaces/common-components';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { DoctorService } from '../../_services/masters/doctor.service';
import * as _ from 'lodash';
import { CodeMasterService } from '../../_services/common/code-master.service';
import { ICodeMaster } from '../../_interfaces/code-master';
import { element } from '@angular/core/src/render3';
import { Element } from '@angular/compiler';
import { DatePipe } from '@angular/common';
import { parseNumber } from '@telerik/kendo-intl';
import { Subject } from 'rxjs/Subject';
import { error } from 'util';
import { ICheckPatientVisit } from '../../_interfaces/patient-visit-queue';

// Requried variables are declared

const Yes = 'YES';
const No = 'NO';
const NewPatientAlert = 'NEWPATIENTALERT';
const PatientMigrationAlert = 'PATIENTMIGRATIONALERT';
const Scheduler = 'Scheduler';
const MoveToQueue = 'MOVETOQUEUE';
const False = 'False';
const CONTENTTYPE = 'image/jpg';
const APH = 'APH';
const CVI = 'CVI';
const NSW = 'NSW';
const CancelAppointment = 'Cancel Appointment';
const currentYear = new Date().getFullYear();
const CameraIcon = 'un-CameraIcon';
const NoCameraIcon = 'un-NoCameraIcon';
const HalfStarIcon = 'un-halfStarIcon';
const FullStarIcon = 'un-FullStarIcon';
const Sucess = 'SUCCESS';
const Name = 'NAME';
const Pin = 'PIN';
const MobileNumber = 'MOBILENUMBER';
const Patient = 'PATIENT';
const parseAdjust = (eventDate: string): Date => {
  const date = new Date(eventDate);
 // date.setFullYear(currentYear);
  return date;
};

// initializing required telerik components

@Component({
  selector: 'app-scheduler',
  templateUrl: 'scheduler.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [`
.kendo-scheduler .k-event, .k-event {

    }
kendo-scheduler .k-event>div, .k-event>div {
      height: 100%;
      position: absolute;
      width: 100%;
      border:.05rem solid;
      border-left:.5rem solid;
    }
    .AAC{
      background-color: #DDEBF7;
      border-color: #2F75B5 !important;
    }
    .ACF{
      background-color: #E2EFDA;
      border-color: #548235 !important;
    }
    .APH{
      background-color: #F2F2F2;
      border-color: #A6A6A6 !important;
    }
    .CNR{
      background-color: #FCE4D6;
      border-color: #C65911 !important;
    }
    .CVI{
      background-color: #F9D9D9;
      border-color: #FF0000 !important;
    }
    .YTC{
      background-color: #FFF2CC;
      border-color: #E1C000 !important;
    }
   .NSW{
      background-color: #FFFCA2;
      border-color: #FFF700 !important;
    }
kendo-scheduler .k-event .k-event-actions .k-icon, .k-event .k-event-actions .k-icon{
display: none !important;
}
.nonWorking {
 background-color: rgb(232, 232, 232);
}
.alternateColor{
background-color: rgba(33,37,41,.03);
}
 .hover:hover, tr.hover:hover{
        padding: 8px 12px;
        margin: -8px -12px;
        background-color: #009DDC;
      }
`],
  providers: [GeneralService, SchedulerService, BlockUIComponent, PatientService, DefaultDate, ShortTime, DefaultDateHypen, DoctorService, CodeMasterService]
})

// All the actice classes are explored in this section

export class SchedulerComponent implements OnInit, AfterContentInit, defaultimport.IDefaults {
  @ViewChild('patientSearch') patientSearch;
  @ViewChild('scheduler') scheduler;
  public LogInUserId: number;
  public currentClinicId: number;
  public IsDoctorLogin: boolean;
  public startTime: Number = 7;
  public endTime: Number = 23;
  private option = 'R';
  private schedulerData: any;
  private appointmentData: any;
  private doctorList: any;
  public isDataLoading: boolean = false;
  public selectedDate: Date = new Date();
  public events: SchedulerEvent[] = [];
  public departmentId: number;
  public group: Group = {
    resources: ['Doctor'],
    orientation: 'horizontal'
  };
  public resources: Resource[] = [];
  private noOfResourcePerPage: number = 0;
  public schedulerLoading: boolean = false;
  public showCancelVisit: boolean = false;
  public expandAdvancedSearchPanel: boolean = false;
  public searchToggleButtonName: string;
  public showCamera: string;
  public showCameraText: string;
  public showAppointment: string;
  public appointmentTypeList: any = [];
  public appointmentStatusList: any = [];
  public appointmentMultislectStatusList: any = [];
  public appointmentType: any = [];
  public appointmentStatus: any = [];
  public schedulerSetting: any;
  public showAddEventDialog: boolean = false;
  public selectedResource: string = '';
  public selectedResourceSpeciality: string = '';
  public appointmentStartDate: Date = new Date();
  public showNewPatient: boolean = false;
  public patientSerachParamsValue: any;
  public patientMobileNumber: string;
  public patientName: string;
  public requestedBy: string;
  public appointmentRemarks: string;
  public patientInfoList: IPatientSearchInfo;
  public patientBannerConfiguration: PatientBannerConfiguration;
  public patientOverdueRemittance: IOverDueCollection[];
  public patientVisitLogParams: any;
  public sendEmail: boolean = true;
  public sendSms: boolean = true;
  public smsNumber: string = '';
  public email: string = '';
  public durationMinute: string = '';
  public durationHour: string = '';
  public doctorId: number = 0;
  private doctorUserId: number = 0;
  private specialityId: number = 0;
  public appointmentDateTime: Date;
  private eventAppointmentType: string;
  private patientSearchInfo: IPatientSearchInfo;
  public appSlotDuration: number = 15;
  public showRecheduleDialog: boolean = false;
  public patientPin: number = 0;
  public rescheduleAppointmentStatus: string = '';
  public appointmentId: number = 0;
  public showAppointmentInfo: boolean = false;
  public appointmenEndTime: Date;
  public isInsurance: string = '';
  public appointmentCreatedBy: string = '';
  public patientDateOfBirth: Date;
  public patientGender: string = '';
  public patientAppointmentType: string = '';
  private moveToQueueClick: boolean = false;
  private moreButtonClick: boolean = false;
  public alertDialogData: any = {
    title: 'CommonModule.Confirmation',
    translatedMessage: '',
    buttonValues: [{ text: 'CommonModule.YesAction', alertId: '', buttonId: '' },
    { text: 'CommonModule.NoAction', alertId: '', buttonId: '' }, { text: 'CommonModule.Cancel', alertId: '', buttonId: '' }],
    message: ''
  };
  public showAlert: boolean = false;
  public showAlertDialog: boolean = false;
  public alertMsg: any;
  private cashOrInsuranceList: any;
  public showUserInfo: boolean = false;
  public statusList: any;
  public currentDate: Date = new Date();
  public enableRemarksPopUp: boolean = false;
  public remarksInput: RemarksInput = new RemarksInput();
  public multiselectDoctorList: any;
  private multiSelectDoctorSourceList: any;
  public multiSelectDoctorValue: any = [];
  public showSettingPopup: boolean = false;
  public subIntervalsSetting: number = 0;
  public shrinkDisplaytext: string = '';
  public genderEnable: boolean = false;
  public anchor: any;
  private isOnLoad: boolean = true;
  public isShrink: boolean = false;
  public screenHeight: number = 600;
  public AppointmentBookingHeight: number = 600;
  private selectedView: string = 'day';
  private selectedDateRange: any;
  public selectedAppointmentType: any;
  private clinicList: any;
  private clinicId: number = 0;
  public validationMessage: string = '';
  public openAppointmentReport: boolean = false;
  public anchorAlign = { horizontal: 'right', vertical: 'bottom' };
  public popupAlign = { horizontal: 'right', vertical: 'top' };
  public showMoreOption: boolean = false;
  public eventAppointmentTypeList: any;
  private dragClick: boolean = false;
  private doctorCount: number = 1;
  public showNext: boolean = false;
  public showPrevious: boolean = false;
  private userCanScheduleUser: boolean = false;
  public procedureList: any = [];
  private sourceProcedureList: any = [];
  public appointmentProcedureList: any = [];
  public showProcedureList: boolean = false;
  public clinicInfo: any;
  public appointmentSettingErrorText: String = '';
  public scrollTime: String = new DatePipe('en-US').transform(new Date(), 'HH:mm');
  public loadingProcedure: boolean = false;
  public mobileNumberLength: number;
  public appointmentStatusColor: string = '';
  private shrinkTimeInMinutes: number = 0;
  public shrinkButtonTilte: String = '';
  private isFirstTimeClick: boolean = false;
  public enableCurrentTime: boolean = true;
  public enableSaveButton: boolean = true;
  public enableAddToQueue: boolean = true;
  public rescheduleSaveButton: boolean = true;
  private beforeRescheduleStatus: string = '';
  private subject: Subject<string> = new Subject();
  public webservicecall;
  public searchStatus: string;
  public selectedEvent: any;
  public doctorWorkingHour: any;
  private alternateColor: boolean = false;
  private previousMinutes: number;
  private deleteappointmentProcedureList: any = [];
  private isSendSms: boolean = false;
  private previousAppointmentDate: Date;
  ngAfterContentInit(): void {
    this.screenHeight = window.innerHeight - 190;
    this.AppointmentBookingHeight = window.outerHeight - 250;
  }
  public getSlotClass = (args: SlotClassArgs) => {
    const hour = args.start.getHours();
    const minutes = args.start.getMinutes();
    const dateString = new DatePipe('en-US').transform(args.start, 'MM/dd/yyyy');

    // here the current date & the user required dates are compared

    for (let resourceIndex = 0; resourceIndex < args.resources.length; resourceIndex++) {
      if (this.doctorWorkingHour) {
        const doctorWorkingHours = this.doctorWorkingHour.filter((s) => s.DoctorId === args.resources[resourceIndex].value && s.Start.indexOf(dateString) !== -1);
        if (!doctorWorkingHours[0]) {
          return {
            nonWorking: !args.isAllDay && (hour >= 0 || hour < 24)
          };
        } else {
          if (args.start < new Date(doctorWorkingHours[0].Start) || args.start > new Date(doctorWorkingHours[0].End)) {
            return {
              nonWorking: !args.isAllDay && args.start
            };
          } else {
            if (minutes === 0) {
              this.alternateColor = false;
            } else {
              if (this.previousMinutes === minutes) {
                this.alternateColor = this.alternateColor ? false : true;
              }
            }
            this.previousMinutes = minutes;
            if (this.alternateColor) {
              this.alternateColor = false;
              return {
                alternateColor: !args.isAllDay && args.start,
                hover: true
              };
            } else {
              this.alternateColor = true;
              return {
                hover: true
              };
            }
          }
        }
      }
    }
  }
  @HostListener('document:click', ['$event'])
  clickAction(event) {
    if (this.eRef.nativeElement.contains(event.target)) {
      const elementId = event.target.id;
      const searchId = 'statusButton';
      if (elementId.search(searchId) === 0) {
        this.showAppointmentInfo = false;
      } else {
        let isPathExist: boolean = false;
        if (event.path) {
          for (let pathIndex = 0; pathIndex < event.path.length; pathIndex++) {
            const pathId = event.path[pathIndex].id;
            if (pathId && pathId.search('infoButton') === 0) {
              isPathExist = true;
              break;
            }
          }
          if (isPathExist) {
            this.showMoreOption = false;
          } else {
            this.showAppointmentInfo = false;
            this.showMoreOption = false;
          }
        } else {
          if (this.showAppointmentInfo) {
            if (!this.isFirstTimeClick) {
              this.showAppointmentInfo = false;
            }
            this.isFirstTimeClick = this.isFirstTimeClick ? false : this.isFirstTimeClick;
          } else {
            this.showMoreOption = false;
          }
        }

      }
    }
  }


  constructor(private generalService: GeneralService,
    private schedulerService: SchedulerService,
    private blockUIComponent: BlockUIComponent,
    private patientService: PatientService,
    private toastr: ToastrManager,
    private defaultDate: DefaultDate,
    private shortTime: ShortTime,
    private defaultDateHypen: DefaultDateHypen,
    private menuMasterService: MenuMasterService,
    private router: Router,
    private domSanitizer: DomSanitizer,
    private eRef: ElementRef,
    private doctorService: DoctorService,

    // required CodeMasterService values are intialized here

    private codeMasterService: CodeMasterService) {

  }

// fetching the configurations

  public async ngOnInit() {
    this.subject.debounceTime(300).distinctUntilChanged().subscribe(searchTextValue => {
      this.handleProcedureSearch(searchTextValue);
    });
    await this.assignDefaultVariableData();
    await this.fetchCodeMasterConfiguration();
  }
  // once the action gets completed destroy event gets triggered
  // tslint:disable-next-line:use-life-cycle-interface
  public ngOnDestroy() {
    this.subject.unsubscribe();
  }

  // Masted data configuration and their setup are categorized in this section

  public async fetchCodeMasterConfiguration() {
    const params = {
      clinicId: this.currentClinicId,
      userId: this.LogInUserId
    };
    const data = await this.schedulerService.GetAppointmentCodeMaster(params).toPromise();
    if (data) {
      this.schedulerSetting = data;
      this.appointmentTypeList = data.AppointmentType;
      this.eventAppointmentTypeList = data.AppointmentType.filter((s) => s.Key3 !== No);
      this.appointmentStatusList = data.AppointmentStatus.filter((s) => s.Value1 !== APH && s.Value1 !== NSW);
      this.appointmentMultislectStatusList = data.AppointmentStatus;
      this.noOfResourcePerPage = data.DoctorCount.Number1;
      this.subIntervalsSetting = data.ShrinkTimeSlot.Value2;
      this.shrinkTimeInMinutes = data.ShrinkTimeSlot.Value1;
      this.shrinkDisplaytext = data.SlotDisplayValues.Value2;
      if (this.isShrink) {
        this.appSlotDuration = this.shrinkTimeInMinutes;
      }
      this.genderEnable = data.DisplayText.Value2 === Yes ? true : false;
      this.statusList = data.AppointmentStatus.filter((s) => s.Value1 !== APH && s.Value1 !== NSW);
      this.showCancelVisit = data.ShowCancelledVisit.Value1 === Yes ? true : false;
      this.showProcedureList = this.schedulerSetting.ShowProcedure.Value1 === Yes ? true : false;
      this.mobileNumberLength = this.schedulerSetting.ContactMandatoryField.Value1 === Yes ? this.schedulerSetting.ContactMandatoryField.Number1 : 60;
      if (this.isOnLoad) {
        this.isOnLoad = false;
        if (this.departmentId) {
          this.getAppointment();
        }
      } else {
        this.applyFilterForAppointment();
      }
    }
  }

  // Assiging the Default values for the declared variables are happened here

  public assignDefaultVariableData(): void {
    this.LogInUserId = this.generalService.getCurrentUserId();
    this.currentClinicId = this.generalService.getCurrentClinicId();
    this.IsDoctorLogin = this.generalService.isDoctorLogin();
    this.userCanScheduleUser = this.generalService.getCurrentUserDetails().UDR_CAN_SCHEDULE_USERS ? true : false;
    if (this.IsDoctorLogin && !this.userCanScheduleUser) {
      this.doctorId = this.generalService.getCurrentUserDetails().DoctorId;
      this.doctorUserId = this.LogInUserId;
      this.specialityId = this.generalService.getCurrentUserDetails().SpecialityId ? this.generalService.getCurrentUserDetails().SpecialityId : 0;
      this.departmentId = this.generalService.getCurrentUserDetails().DepartmentId;
    } else {
      this.doctorId = 0;
      this.doctorUserId = 0;
      this.clinicId = this.currentClinicId;
    }
    this.searchToggleButtonName = QueueStyleClassEnum.DownIconClass;
    this.showCamera = CameraIcon;
    this.showCameraText = 'CommonModule.ShowDoctorDetails';
    this.showAppointment = FullStarIcon;
    this.patientSerachParamsValue = {
      clinicId: this.currentClinicId,
      userId: this.LogInUserId
    };
    this.durationHour = '00';
    this.durationMinute = '15';
    this.shrinkButtonTilte = 'AppointmentModule.Shrink';
    this.events.push({
      id: null,
      dataItem: '',
      start: new Date(),
      startTimezone: null,
      end: new Date(),
      endTimezone: null,
      isAllDay: false,
      title: '',
      description: '',
      recurrenceRule: '',
      recurrenceId: null
    });
    if (this.IsDoctorLogin && !this.userCanScheduleUser) {
      this.group = {
        resources: ['Clinic'],
        orientation: 'horizontal'
      };
      this.resources = [{
        name: 'Clinic',
        data: [{ text: '', value: '', color: '' }],
        field: 'clinicId',
        valueField: 'value',
        textField: 'text'
      }];
    } else {
      this.resources = [{
        name: 'Doctor',
        data: [{ text: '', value: '', color: '' }],
        field: 'appointmentUserId',
        valueField: 'value',
        textField: 'text'
      }];
    }
  }

  // This function is used to list all the available Appointment Lists to the doctor

  private getAppointment(): void {
    this.isDataLoading = true;
    let appointmentViewStartDate: Date;
    let appointmentViewEndDate: Date;
    if (this.selectedView === 'day') {
      this.option = 'R';
      appointmentViewStartDate = this.selectedDate;
      appointmentViewEndDate = this.selectedDate;
    } else {
      this.option = 'R';
      appointmentViewStartDate = this.selectedDateRange.start;
      appointmentViewEndDate = this.selectedDateRange.end;
    }
    const appointmentParam = {
      clinicId: this.currentClinicId,
      day: this.selectedDate.getDay(),
      month: this.selectedDate.getMonth(),
      year: this.selectedDate.getFullYear(),
      option: this.option,
      doctorId: this.doctorId,
      cdtag: this.IsDoctorLogin && !this.userCanScheduleUser ? 'D' : 'C',
      from: appointmentViewStartDate.toDateString(),
      to: appointmentViewEndDate.toDateString(),
      departmentId: this.departmentId,
      userId: this.LogInUserId,
      isCancelVisit: this.showCancelVisit ? Yes : No
    };
    this.schedulerService.GetAppointment(appointmentParam).subscribe(data => {
      this.schedulerData = data;
      if (this.schedulerData) {
        this.appointmentData = this.schedulerData.Appointments;
        this.cashOrInsuranceList = this.schedulerData.CashInsurance;
        this.doctorList = this.schedulerData.SchedulerFilter.Doctor;
        this.clinicList = this.schedulerData.SchedulerFilter.Clinic;
        this.doctorWorkingHour = this.schedulerData.WorkingDays;
        this.applyFilterForAppointment();
      }
      this.isDataLoading = false;
      if (this.selectedDate.toDateString() === new Date().toDateString()) {
        this.scrollTime = new DatePipe('en-US').transform(new Date(), 'HH:mm');
      }
    });

  }

  // Applying Filters over the appointments (i.e) filter by date,doctor,speciality are happened here

  private applyFilterForAppointment(): void {
    let doctorImage: any = '';
    const resourceDataList: any = [];
    const multiSelectDoctorIdList: any = [];
    const multiSelectAppointmentTypeList: any = [];
    const multiSelectappointmentStatusList: any = [];
    for (let doctorIdIndex = 0; doctorIdIndex < this.multiSelectDoctorValue.length; doctorIdIndex++) {
      multiSelectDoctorIdList.push(this.multiSelectDoctorValue[doctorIdIndex].UserId);
    }
    if (multiSelectDoctorIdList && multiSelectDoctorIdList.length > 0) {
      this.doctorList = _.filter(this.schedulerData.SchedulerFilter.Doctor, (item) => {
        return multiSelectDoctorIdList.indexOf(item.DoctorId) > -1;
      });
    } else {
      this.doctorList = this.schedulerData.SchedulerFilter.Doctor;
    }
    for (let typeIndex = 0; typeIndex < this.appointmentType.length; typeIndex++) {
      multiSelectAppointmentTypeList.push(this.appointmentType[typeIndex].Value1);
    }
    for (let statusIndex = 0; statusIndex < this.appointmentStatus.length; statusIndex++) {
      multiSelectappointmentStatusList.push(this.appointmentStatus[statusIndex].Value1);
    }
    if (this.IsDoctorLogin && !this.userCanScheduleUser) {
      const resourceIdList: any = [];
      for (let clinicIndex = 0; clinicIndex < this.clinicList.length; clinicIndex++) {
        if (clinicIndex < this.noOfResourcePerPage) {
          const clinicParam = {
            text: this.clinicList[clinicIndex].ClinicLongName,
            value: this.clinicList[clinicIndex].ClinicId,
            color: '',
            speciality: '',
            specialityId: 0,
            cash: 0,
            image: '',
            insurance: 0
          };
          resourceDataList.push(clinicParam);
          resourceIdList.push(this.clinicList[clinicIndex].ClinicId);
        } else {
          break;
        }
      }
      this.resources = [{
        name: 'Clinic',
        data: resourceDataList,
        field: 'clinicId',
        valueField: 'value',
        textField: 'text'
      }];
      if (resourceIdList.length > 0) {
        this.appointmentData = _.filter(this.schedulerData.Appointments, (item) => {
          return resourceIdList.indexOf(item.ClinicId) > -1;
        });
      }
    } else {
      if (this.doctorList.length > this.doctorCount * this.noOfResourcePerPage) {
        this.showNext = true;
      } else {
        this.showNext = false;
      }
      this.showPrevious = this.doctorCount > 1 ? true : false;
      const resourceIdList: any = [];
      for (let doctorIndex = (this.doctorCount - 1) * this.noOfResourcePerPage; doctorIndex < this.doctorList.length; doctorIndex++) {
        const insuranceCount = this.cashOrInsuranceList.filter((s) => s.DoctorId === this.doctorList[doctorIndex].DoctorId);
        const contentType = CONTENTTYPE;
        const blob = this.generalService.getBlobData(this.doctorList[doctorIndex].Image, contentType);
        const objectURL = URL.createObjectURL(blob);
        doctorImage = this.domSanitizer.bypassSecurityTrustResourceUrl(objectURL);
        if (doctorIndex < this.doctorCount * this.noOfResourcePerPage) {
          const doctorParam = {
            text: this.doctorList[doctorIndex].DoctorName,
            value: this.doctorList[doctorIndex].DoctorId,
            color: '',
            speciality: this.doctorList[doctorIndex].DoctorSpeciality,
            specialityId: this.doctorList[doctorIndex].SpecialityID,
            cash: insuranceCount.length > 0 ? insuranceCount[0].CashCount : 0,
            image: doctorImage,
            insurance: insuranceCount.length > 0 ? insuranceCount[0].InsuranceCount : 0
          };
          resourceDataList.push(doctorParam);
          resourceIdList.push(this.doctorList[doctorIndex].DoctorId);
        } else {
          break;
        }
      }
      this.resources = [{
        name: 'Doctor',
        data: resourceDataList,
        field: 'appointmentUserId',
        valueField: 'value',
        textField: 'text'
      }];
      if (resourceIdList.length > 0) {
        this.appointmentData = _.filter(this.schedulerData.Appointments, (item) => {
          return resourceIdList.indexOf(item.AppointmentUserId) > -1;
        });
      }
    }
    if (multiSelectAppointmentTypeList.length > 0) {
      this.appointmentData = _.filter(this.appointmentData, (item) => {
        return multiSelectAppointmentTypeList.indexOf(item.AppointmentType) > -1;
      });
    }
    if (multiSelectappointmentStatusList.length > 0) {
      this.appointmentData = _.filter(this.appointmentData, (item) => {
        return multiSelectappointmentStatusList.indexOf(item.StatusShort) > -1;
      });
    }
    this.events = this.appointmentData.map(dataItem => (
      <SchedulerEvent>{
        id: dataItem.AppointmentId,
        dataItem: dataItem,
        start: parseAdjust(dataItem.AppointmentTime),
        startTimezone: null,
        end: parseAdjust(dataItem.AppointmentTimeEnd),
        endTimezone: null,
        isAllDay: false,
        title: dataItem.PatientPin === 0 ? dataItem.PatientFirstName : dataItem.PatientPin + ', ' + this.defaultDate.transform(new Date(dataItem.PatientDateOfBirth)) + ', ' + dataItem.PatientGender,
        recurrenceRule: '',
        recurrenceId: null,
        recurrenceException: '',
        doctorId: dataItem.DoctorId,
        clinicId: dataItem.ClinicId,
        description: dataItem.StatusShort,
        appointmentUserId: dataItem.AppointmentUserId,
        // tslint:disable-next-line:max-line-length
        shrinkTitle: (this.shrinkDisplaytext === Name ? dataItem.PatientFirstName : this.shrinkDisplaytext === Pin ? dataItem.PatientPin : this.shrinkDisplaytext === MobileNumber ? dataItem.PatientMobile : '') + this.genderEnable ? ' /' + dataItem.PatientGender : ''
      }
    ));
    setTimeout(() => {
      const rows = Array.from(document.querySelectorAll('.k-scheduler-content .k-scheduler-table tr'));
      rows.forEach(r => {
        if (r.querySelector('td').classList.contains('hover')) {
          r.classList.add('hover');
        }
      });
    });
  }

   // patients Cancelling Visit/appointment & doctor cancelling the booked appointments

  public showCancelVisitChange(): void {
    this.getAppointment();
  }

  // Searching with speciality,date,patient history with advanced search functionality

  public advancedSearchToggle(): void {
    this.expandAdvancedSearchPanel = !this.expandAdvancedSearchPanel;
    if (this.expandAdvancedSearchPanel) {
      this.searchToggleButtonName = QueueStyleClassEnum.UpIconClass;
    } else {
      this.searchToggleButtonName = QueueStyleClassEnum.DownIconClass;
    }
  }

  // change in department of the patient & doctor

  public departmentChange(event: any): void {
    if (!this.IsDoctorLogin || this.userCanScheduleUser) {
      this.doctorId = 0;
      this.doctorUserId = 0;
      this.doctorCount = 1;
      this.multiSelectDoctorValue = [];
      if (event) {
        if (this.departmentId !== event.DepartmentId) {
          this.departmentId = event.DepartmentId;
        }
      } else {
        this.departmentId = 0;
      }
      if (!this.isOnLoad) {
        this.getAppointment();
      }
    }
  }

  // Appointment Types duration can be changed here from 1 hr to 1/2 & vise versa funcationality

  public onAppointmentTypeChange(value: any): void {
    this.applyFilterForAppointment();
  }

  // Status of the appointment based on date

  public onAppointmentStatusChange(value: any): void {
    this.applyFilterForAppointment();
  }

  // Filters for Appointments with doctor & department wise

  public appointmentTypeFilterChange(value: any): void {
    this.appointmentTypeList = this.schedulerSetting.AppointmentType.filter((s) => s.Value1.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  // Filters based on Status on MultiSelect like doctor wise,depatment wise,clinic wise & multiple doctor wise

  public appointmentStatusFilterChangeMultiselect(value: any): void {
    this.appointmentMultislectStatusList = this.schedulerSetting.AppointmentStatus.filter((s) => s.Value2.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  // Filters based on Status like booked,cancelled,open slots

  public appointmentStatusFilterChange(value: any): void {
    this.appointmentStatusList = this.filterAppointmentSelectedStatus(this.beforeRescheduleStatus, true);
    this.appointmentStatusList = this.appointmentStatusList.filter((s) => s.Value2.toLowerCase().indexOf(value.toLowerCase()) !== -1 && s.Value1 !== APH && s.Value1 !== NSW);
  }

  // changing the status of the appointment

  public appointmentStatusValueChange(value: any): void {
    this.showAppointmentInfo = false;
    this.onSelectStatus(value.Value3);
  }

  // Doctor level Filters on booked Appointments

  public doctorFilterChange(value: any): void {
    this.multiselectDoctorList = this.multiSelectDoctorSourceList.filter((s) => s.DoctorFirstName.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  // Listing the slots along with the appointments
  public async slotDblClickHandler(event: any) {
    await this.clearAppointment();
    if (!event.isAllDay && !event.slot.nonWorkHour && event.slot.element.nativeElement.className.indexOf('nonWorking') === -1) {
      this.selectedResource = event.resources[0].text;
      this.selectedResourceSpeciality = event.resources[0].speciality;
      this.appointmentStartDate = event.start;
      if (!this.IsDoctorLogin || this.userCanScheduleUser) {
        this.doctorUserId = event.resources[0].value;
        this.specialityId = event.resources[0].specialityId;
      } else {
        this.clinicId = event.resources[0].value;
      }
      this.appointmentDateTime = event.start;
      if (!this.onCheckSlotAvailability(event.start, event.end, event.sender.events, this.doctorUserId, 0)) {
        this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentSlotValidation',
          // tslint:disable-next-line: max-line-length
          { Date: this.defaultDate.transform(this.appointmentDateTime), ShortTime: this.shortTime.transform(this.appointmentDateTime), DoctorName: this.selectedResource, Speciality: this.selectedResourceSpeciality }).then(translatedMessage => {
            this.showCommonAlert('CommonModule.Validation', '', translatedMessage, MessageTypeenum.WARNING);
          });

      } else {
        const currentDate = new Date();
        const eventSartTime = event.start.getHours();
        const workDayEndTime = event.slot.workDayEnd.getHours();
        if (event.start > currentDate && eventSartTime < workDayEndTime) {
          if (this.doctorUserId !== 0) {
            this.showAddEventDialog = true;
          }
        }
      }
    }
  }

  // Creation of New Appointment

  public addAppointmentClose(): void {
    this.showAddEventDialog = false;
    this.clearAppointment();
  }

  // Saving the created Appointment

  public saveAppointment(): void {
    this.enableSaveButton = false;
    this.validationMessage = '';
    if (this.selectedAppointmentType) {
      if (this.durationHour !== '' && this.durationMinute !== '') {
        if (this.showNewPatient) {
          if (this.patientName.replace(/\s/g, '').length === 0) {
            this.validationMessage = 'AppointmentModule.EnterName';
          } else {
            if (this.patientMobileNumber.length === 0 && this.mobileNumberLength === 60) {
              this.validationMessage = 'AppointmentModule.EnterMobileNumber';
            } else if (this.mobileNumberLength < 60 && this.patientMobileNumber.length < this.mobileNumberLength) {
              this.validationMessage = 'AppointmentModule.MobileNumberValidation';
              this.generalService.getMultiTranslatedMessage('AppointmentModule.MobileNumberValidation',
                { Length: this.mobileNumberLength }).then(translatedMessage => {
                  this.validationMessage = translatedMessage;
                });
            }
          }
        } else {
          if (!this.patientSearchInfo) {
            this.validationMessage = 'AppointmentModule.PatientSelect';
          }
        }
      } else {
        this.validationMessage = 'AppointmentModule.EnterDuraion';
      }
    } else {
      this.validationMessage = 'AppointmentModule.SelectAppointmentType';
    }
    if (this.validationMessage.length === 0) {
      if (this.appointmentDateTime == null || this.appointmentDateTime === undefined) {
        this.appointmentDateTime = this.appointmentStartDate;
      }
      let appointmentEnd: Date = new Date(this.appointmentDateTime.getTime() + (Number(this.durationHour) * 60 * 60 * 1000));
      appointmentEnd = new Date(appointmentEnd.getTime() + Number(this.durationMinute) * 60000);
      if (this.checkWorkingHour(this.doctorUserId, this.appointmentDateTime, appointmentEnd)) {
        // tslint:disable-next-line: max-line-length
        if (!this.onCheckAppointmentAvailability(0, this.appointmentDateTime, appointmentEnd, this.appointmentData, this.doctorUserId, this.showNewPatient ? 0 : this.patientSearchInfo.PatientPin, this.patientMobileNumber)) {
          const appointmentParam: any = {
            userId: this.LogInUserId,
            doctorId: this.doctorUserId,
            clinicId: this.clinicId,
            specialityId: this.specialityId,
            preAuth: '',
            pin: this.showNewPatient ? 0 : this.patientSearchInfo.PatientPin,
            firstName: this.showNewPatient ? this.patientName : this.patientSearchInfo.PatientFirstName,
            mobileNo: this.showNewPatient ? this.patientMobileNumber : this.patientSearchInfo.PatientMobile,
            requestedby: this.requestedBy,
            notes: this.appointmentRemarks ? this.appointmentRemarks : '',
            sms: this.smsNumber,
            email: this.email,
            isFollowup: false,
            duration: this.durationHour + ':' + this.durationMinute,
            appointmentTime: this.defaultDateHypen.transform(this.appointmentDateTime) + ' ' + this.shortTime.transform(this.appointmentDateTime),
            isSms: this.sendSms,
            isEmail: this.sendEmail,
            appointmentType: this.eventAppointmentType,
            appointmentId: 0,
            departmentId: this.departmentId !== 0 ? this.departmentId : this.getResourceDetails(this.doctorUserId).DepartmentId
          };
          let messageStr: string = '';
          this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentSaved',
            // tslint:disable-next-line: max-line-length
            { PatientName: this.showNewPatient ? this.patientName : this.patientSearchInfo.PatientFirstName, DoctorName: this.selectedResource, Date: this.defaultDate.transform(this.appointmentDateTime), Time: this.shortTime.transform(this.appointmentDateTime) }).then(translatedMessage => {
              messageStr = translatedMessage;
            });
          this.schedulerService.SaveAppointment(appointmentParam).subscribe(data => {
            if (this.showProcedureList && this.appointmentProcedureList.length > 0) {
              for (let procedureIndex = 0; procedureIndex < this.appointmentProcedureList.length; procedureIndex++) {
                this.appointmentProcedureList[procedureIndex].APM_ID = data.Message;
                this.appointmentProcedureList[procedureIndex].ATD_SEQ = procedureIndex + 1;
                this.appointmentProcedureList[procedureIndex].ATR_CREATEDBY = this.LogInUserId;
              }
              this.schedulerService.SaveAppointmentProcedure(this.appointmentProcedureList).subscribe(procedureData => {
                this.toastr.successToastr(messageStr, this.generalService.onTranslateService('CommonModule.Success'), {
                  toastTimeout: 3000
                });
                this.getAppointment();
                this.clearAppointment();
                this.showAddEventDialog = false;
                this.enableSaveButton = true;
              });
            } else {
              this.toastr.successToastr(messageStr, this.generalService.onTranslateService('CommonModule.Success'), {
                toastTimeout: 3000
              });
              this.getAppointment();
              this.clearAppointment();
              this.showAddEventDialog = false;
              this.enableSaveButton = true;
            }

          });
        } else {
          this.enableSaveButton = true;
          this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentAvailability',
            // tslint:disable-next-line: max-line-length
            { PatientName: this.showNewPatient ? this.patientName : this.patientSearchInfo.PatientFirstName, DoctorName: this.getResourceDetails(this.doctorUserId).DoctorName, Date: this.defaultDate.transform(this.appointmentDateTime), ShortTime: this.shortTime.transform(this.appointmentDateTime) }).then(translatedMessage => {
              this.validationMessage = translatedMessage;
            });
        }
      } else {
        this.enableSaveButton = true;
        this.validationMessage = 'AppointmentModule.AppointmentSlotCheck';
      }
    } else {
      this.enableSaveButton = true;
    }
  }

  // Cancelling the booked Appointment

  public cancelAppointment(): void {
    this.showAddEventDialog = false;
    this.clearAppointment();
  }

  // Clearing the values entered on the input controls

  public clearAppointment(): void {
    this.validationMessage = '';
    this.patientInfoList = undefined;
    this.patientMobileNumber = '';
    this.patientName = '';
    this.smsNumber = '';
    this.email = '';
    this.requestedBy = '';
    this.appointmentRemarks = '';
    this.durationHour = '00';
    this.durationMinute = '15';
    this.sendEmail = true;
    this.sendSms = true;
    this.patientPin = 0;
    this.rescheduleAppointmentStatus = '';
    this.beforeRescheduleStatus = '';
    if (!this.IsDoctorLogin || this.userCanScheduleUser) {
      this.doctorId = 0;
      this.doctorUserId = 0;
      this.specialityId = 0;
    } else {
      this.clinicId = 0;
    }
    this.appointmentDateTime = null;
    this.appointmentId = 0;
    this.appointmenEndTime = null;
    this.isInsurance = '';
    this.appointmentCreatedBy = '';
    this.patientDateOfBirth = null;
    this.patientGender = '';
    this.patientAppointmentType = '';
    this.showNewPatient = false;
    this.eventAppointmentType = Patient;
    for (let typeIndex = 0; typeIndex < this.eventAppointmentTypeList.length; typeIndex++) {
      if (this.eventAppointmentType === this.eventAppointmentTypeList[typeIndex].Value1) {
        this.selectedAppointmentType = this.eventAppointmentTypeList[typeIndex];
        break;
      }
    }
    if (this.patientSearch) {
      this.patientSearchInfo = undefined;
      this.patientSearch.patientInfo = undefined;
    }
    this.appointmentProcedureList = [];
    this.appointmentStatusColor = '';
  }

  // creating & new patient

  public showNewPatientChange(): void {
    if (this.patientSearch) {
      this.patientSearchInfo = undefined;
      this.patientSearch.patientInfo = undefined;
    }
    this.patientInfoList = undefined;
    this.patientMobileNumber = '';
    this.patientName = '';
    this.smsNumber = '';
    this.email = '';
    this.requestedBy = '';
    this.appointmentRemarks = '';
    this.durationHour = '00';
    this.durationMinute = '15';
    this.sendEmail = true;
    this.sendSms = true;
    this.patientPin = 0;
    this.rescheduleAppointmentStatus = '';
    this.beforeRescheduleStatus = '';
    this.validationMessage = '';
    this.appointmentProcedureList = [];
  }

  // Searching the Patients

  public patientSearchreturnValue(e: any): void {
    if (e.PatientPin !== 0) {
      this.patientSearchInfo = e;
      this.onLoadPatientConfigrationData();
      this.patientBannerConfiguration.PatientPin = e.PatientPin;
      this.patientService.GetPatientInfoList(e.PatientPin, this.patientSerachParamsValue).subscribe(data => {
        this.patientInfoList = data;
        if (this.patientInfoList[0].DateOfBirth) {
          this.patientInfoList[0].DateOfBirth = new Date(this.patientInfoList[0].DateOfBirth);
        }
        this.patientBannerConfiguration.IsHideInsurance = true;
        this.patientVisitLogParams = {
          pin: this.patientInfoList[0].PatientPin,
          ClinicId: this.currentClinicId,
          usr_id: this.LogInUserId
        };
      });
    } else {
      this.patientInfoList = undefined;
      if (this.patientBannerConfiguration) {
        this.patientBannerConfiguration.IsHideInsurance = false;
      }
      this.patientOverdueRemittance = undefined;
      this.patientInfoList = undefined;
      this.patientMobileNumber = '';
      this.patientName = '';
      this.smsNumber = '';
      this.email = '';
      this.requestedBy = '';
      this.appointmentRemarks = '';
      this.durationHour = '00';
      this.durationMinute = '15';
      this.sendEmail = true;
      this.sendSms = true;
      this.patientPin = 0;
      this.rescheduleAppointmentStatus = '';
      this.beforeRescheduleStatus = '';
      this.appointmentId = 0;
      this.isInsurance = '';
      this.appointmentCreatedBy = '';
      this.patientDateOfBirth = null;
      this.patientGender = '';
      this.patientAppointmentType = '';
      this.showNewPatient = false;
      if (this.patientSearch) {
        this.patientSearchInfo = undefined;
        this.patientSearch.patientInfo = undefined;
      }
      this.appointmentProcedureList = [];
      this.appointmentStatusColor = '';
    }
  }

  // payment details of the patients including pending and paid amount

  public onShowRemittanceDetails(e: any): void {
    this.patientOverdueRemittance = e;
  }

  // Configuring to list the required columns based on the clinic setup,based on the configuration the columns are listed

  private onLoadPatientConfigrationData(): void {
    this.patientBannerConfiguration = new PatientBannerConfiguration();
    this.patientBannerConfiguration.ShowOverDueContent = true;
    this.patientBannerConfiguration.ShowAdvanceContent = true;
  }

  // Types of appointment are created here

  public onEventCreationAppointmentTypeChange(value: any): void {
    if (value) {
      this.eventAppointmentType = value.Value1;
    }
  }

  // Refresh the slots based on date

  public refreshAppointmentQueue(): void {
    this.getAppointment();
  }

  // Changing the appointment Date

  public async appointmentDateChange(event: DateChangeEvent) {
    this.selectedDate = event.selectedDate;
    this.selectedDateRange = event.dateRange;
    this.selectedView = event.sender.selectedView.name;
    if (!this.isOnLoad) {
      if (!this.isDataLoading) {
        this.getAppointment();
      }
    }
    this.scrollToCurrentTime();
  }

  // Listing the appointment based on slots in a queue

  public addToQueue(event: any, closeInfoPopUp): void {
    this.showAppointmentInfo = closeInfoPopUp ? false : this.showAppointmentInfo;
    this.clearAppointment();
    this.enableAddToQueue = false;
    const patientMigrationCompleted = event.dataItem.dataItem.IsMigrationData;
    this.patientPin = event.dataItem.dataItem.PatientPin;
    this.patientName = event.dataItem.dataItem.PatientFirstName;
    this.appointmentId = event.dataItem.dataItem.AppointmentId;
    this.appointmentDateTime = new Date(event.dataItem.dataItem.AppointmentTime);
    this.doctorId = event.dataItem.dataItem.DoctorId;
    this.doctorUserId = event.dataItem.dataItem.AppointmentUserId;
    this.specialityId = this.IsDoctorLogin && !this.userCanScheduleUser ? this.specialityId : this.getResourceDetails(event.dataItem.dataItem.AppointmentUserId).SpecialityID;
    this.patientMobileNumber = event.dataItem.dataItem.PatientMobile;
    this.moveToQueueClick = true;
    this.schedulerService.GetOverdue(this.patientPin, this.clinicId, this.LogInUserId).subscribe(data => {
      if (data[0].OverdueAmt > 0) {
        this.generalService.getMultiTranslatedMessage('AppointmentModule.OverdueAlert',
          { PatientName: this.patientName, Amount: data[0].OverdueAmt }).then(translatedMessage => {
            this.showCommonAlertDialog('CommonModule.Confirmation', translatedMessage, [{ text: 'CommonModule.YesAction', alertId: NewPatientAlert, buttonId: Yes },
            { text: 'CommonModule.NoAction', alertId: NewPatientAlert, buttonId: No }], '');
          });
        this.showCommonAlertDialog('CommonModule.Confirmation', '', [{ text: 'CommonModule.YesAction', alertId: MoveToQueue, buttonId: Yes },
        { text: 'CommonModule.NoAction', alertId: NewPatientAlert, buttonId: No }], 'AppointmentModule.MoveToQueue');
      } else {
        if (this.patientPin !== 0) {
          if (patientMigrationCompleted) {
            this.showCommonAlertDialog('CommonModule.Confirmation', '', [{ text: 'CommonModule.YesAction', alertId: MoveToQueue, buttonId: Yes },
            { text: 'CommonModule.NoAction', alertId: NewPatientAlert, buttonId: No }], 'AppointmentModule.MoveToQueue');
          } else {
            if (this.schedulerSetting.MoveToPatientRegister.Value1 === Yes) {
              this.generalService.getMultiTranslatedMessage('AppointmentModule.ValidateIncompletePatient',
                { PatientName: this.patientName }).then(translatedMessage => {
                  this.showCommonAlertDialog('CommonModule.Confirmation', translatedMessage, [{ text: 'CommonModule.YesAction', alertId: NewPatientAlert, buttonId: Yes },
                  { text: 'CommonModule.NoAction', alertId: NewPatientAlert, buttonId: No }], '');
                });
            } else {
              this.showCommonAlertDialog('CommonModule.Confirmation', '', [{ text: 'CommonModule.YesAction', alertId: MoveToQueue, buttonId: Yes },
              { text: 'CommonModule.NoAction', alertId: NewPatientAlert, buttonId: No }], 'AppointmentModule.MoveToQueue');
            }
          }
        } else {
          this.generalService.getMultiTranslatedMessage('AppointmentModule.ValidateNewPatient',
            { PatientName: this.patientName }).then(translatedMessage => {
              this.showCommonAlertDialog('CommonModule.Confirmation', translatedMessage, [{ text: 'CommonModule.YesAction', alertId: PatientMigrationAlert, buttonId: Yes },
              { text: 'CommonModule.NoAction', alertId: PatientMigrationAlert, buttonId: No }], '');
            });
        }
      }
    });
    this.enableAddToQueue = true;
  }

  // Other Actions includes critical or mandatory slot booking based on patient emergency

  public moreAction(event: any): void {
    this.moreButtonClick = true;
    this.showMoreOption = true;
    this.clearAppointment();
    this.appointmentId = event.dataItem.dataItem.AppointmentId;
    this.appointmentRemarks = event.dataItem.dataItem.Remarks;
    this.doctorUserId = event.dataItem.dataItem.AppointmentUserId;
    this.statusList = this.filterAppointmentSelectedStatus(event.dataItem.dataItem.StatusShort, false);
  }

  // Status of the Appointment whether patient on queue or delay infos

  private filterAppointmentSelectedStatus(selectedStatus: string, includeSelectedStatus: boolean): any {
    const currentStatus = this.schedulerSetting.AppointmentStatus.filter((s) => s.Value1 === selectedStatus);
    let statusIdList = [];
    if (currentStatus[0].String2) {
      statusIdList = currentStatus[0].String2.split(',');
    }
    let tempStatusList = this.schedulerSetting.AppointmentStatus.filter((s) => s.Value1 !== APH && s.Value1 !== NSW && s.Value1 !== selectedStatus);
    if (includeSelectedStatus) {
      statusIdList.push(currentStatus[0].Number1.toString());
      tempStatusList = this.schedulerSetting.AppointmentStatus.filter((s) => s.Value1 !== APH && s.Value1 !== NSW);
    }
    const filteredStatusList = _.filter(tempStatusList, (item) => {
      return statusIdList.indexOf(item.Number1.toString()) > -1;
    });
    return filteredStatusList;
  }

  // Appointment page expand and minimize region

  public expandAndShrink(): void {
    this.isShrink = this.isShrink ? false : true;
    if (this.isShrink) {
      this.appSlotDuration = this.shrinkTimeInMinutes;
      this.showAppointment = HalfStarIcon;
      this.shrinkButtonTilte = 'AppointmentModule.Expand';
    } else {
      this.appSlotDuration = 15;
      this.showAppointment = FullStarIcon;
      this.shrinkButtonTilte = 'AppointmentModule.Shrink';
    }
    this.scrollToCurrentTime();
  }

  // drag or moving the Appointment controls over the page

  public async dragAppointment(event: any) {
    this.dragClick = true;
    this.showAppointmentInfo = false;
    // tslint:disable-next-line: max-line-length
    if (event.event.dataItem.dataItem.StatusShort !== CVI && event.event.dataItem.dataItem.StatusShort !== APH && event.event.dataItem.dataItem.StatusShort !== NSW && this.checkWorkingHour(event.resources.appointmentUserId, event.start, event.end)) {
      // tslint:disable-next-line: max-line-length
      if (this.onCheckSlotAvailability(event.start, event.end, event.sender.events, this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.dataItem.appointmentUserId, event.event.dataItem.dataItem.AppointmentId)) {
        // tslint:disable-next-line: max-line-length
        if (!this.onCheckAppointmentAvailability(event.event.dataItem.dataItem.AppointmentId, event.start, event.end, this.appointmentData, this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.resources.appointmentUserId, event.event.dataItem.dataItem.PatientPin, event.event.dataItem.dataItem.PatientMobile)) {
          this.clearAppointment();
          const appointmentParam = {
            appointmentId: event.event.dataItem.dataItem.AppointmentId,
            userId: this.LogInUserId,
            doctorId: this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.resources.appointmentUserId,
            specialityId: this.IsDoctorLogin && !this.userCanScheduleUser ? this.specialityId : this.getResourceDetails(event.resources.appointmentUserId).SpecialityID,
            duration: this.durationCalculation(new Date(event.event.dataItem.dataItem.AppointmentTime).valueOf(), new Date(event.event.dataItem.dataItem.AppointmentTimeEnd).valueOf()),
            appointmentDate: this.defaultDateHypen.transform(new Date(event.start)),
            remarks: event.event.dataItem.dataItem.Remarks ? event.event.dataItem.dataItem.Remarks : '',
            time: this.shortTime.transform(new Date(event.start)),
            status: event.event.dataItem.dataItem.StatusShort,
            clinicId: this.IsDoctorLogin && !this.userCanScheduleUser ? event.resources.clinicId : this.clinicId,
            departmentId: this.departmentId !== 0 ? this.departmentId : this.getResourceDetails(event.resources.appointmentUserId).DepartmentId,
            isSendSms: this.isSendSms
          };
          if (event.end >= new Date()) {
            this.updateAppointment(appointmentParam);
          } else {
            this.clearAppointment();
          }
        } else {
          this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentAvailability',
            // tslint:disable-next-line: max-line-length
            { PatientName: event.event.dataItem.dataItem.PatientFirstName, DoctorName: this.getResourceDetails(this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.resources.appointmentUserId).DoctorName, Date: this.defaultDate.transform(event.start), ShortTime: this.shortTime.transform(event.start) }).then(translatedMessage => {
              this.toastr.warningToastr(translatedMessage, this.generalService.onTranslateService('CommonModule.Validation'), {
                toastTimeout: 3000
              });
            });
        }
      } else {
        this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentSlotValidation',
          // tslint:disable-next-line: max-line-length
          { Date: this.defaultDate.transform(event.start), ShortTime: this.shortTime.transform(event.start), DoctorName: this.getResourceDetails(this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.resources.appointmentUserId).DoctorName, Speciality: this.getResourceDetails(this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.resources.appointmentUserId).DoctorSpeciality }).then(translatedMessage => {
            this.toastr.warningToastr(translatedMessage, this.generalService.onTranslateService('CommonModule.Validation'), {
              toastTimeout: 3000
            });
          });
      }
    }
  }

  // rescheduling the booked Appointment

  public async rescheduleAppointment(event: any) {
    this.currentDate = new Date();
    // tslint:disable-next-line: max-line-length
    if (event.event.dataItem.dataItem.StatusShort !== CVI && event.event.dataItem.dataItem.StatusShort !== APH && event.event.dataItem.dataItem.StatusShort !== NSW && (this.defaultDate.transform(new Date(event.event.dataItem.dataItem.AppointmentTime)) === this.defaultDate.transform(this.currentDate) || new Date(event.event.dataItem.dataItem.AppointmentTime) > this.currentDate)) {
      this.clearAppointment();
      this.showAppointmentInfo = false;
      this.showRecheduleDialog = true;
      this.appointmentStartDate = new Date(event.event.dataItem.dataItem.AppointmentTime);
      this.previousAppointmentDate = new Date(event.event.dataItem.dataItem.AppointmentTime);
      this.selectedResource = event.event.dataItem.dataItem.DoctorName;
      this.appointmentDateTime = new Date(event.event.dataItem.dataItem.AppointmentTime);
      if (this.IsDoctorLogin && !this.userCanScheduleUser) {
        this.clinicId = event.event.dataItem.dataItem.DoctorId;
      } else {
        this.doctorId = event.event.dataItem.dataItem.DoctorId;
        this.doctorUserId = event.event.dataItem.dataItem.AppointmentUserId;
        this.specialityId = this.getResourceDetails(event.event.dataItem.dataItem.AppointmentUserId).SpecialityID;
      }
      this.displayDurationCaluculation(new Date(event.event.dataItem.dataItem.AppointmentTime), new Date(event.event.dataItem.dataItem.AppointmentTimeEnd));
      this.patientPin = event.event.dataItem.dataItem.PatientPin;
      this.patientMobileNumber = event.event.dataItem.dataItem.PatientMobile;
      this.patientName = event.event.dataItem.dataItem.PatientFirstName;
      this.rescheduleAppointmentStatus = event.event.dataItem.dataItem.StatusShort;
      this.appointmentStatusList = this.filterAppointmentSelectedStatus(event.event.dataItem.dataItem.StatusShort, true);
      this.beforeRescheduleStatus = event.event.dataItem.dataItem.StatusShort;
      this.appointmentRemarks = event.event.dataItem.dataItem.Remarks;
      this.appointmentId = event.event.dataItem.dataItem.AppointmentId;
      await this.schedulerService.GetAppointmentProcedure(this.appointmentId).subscribe(proceduredata => {
        this.appointmentProcedureList = proceduredata;
      });
      if (this.patientPin !== 0) {
        this.onLoadPatientConfigrationData();
        this.patientBannerConfiguration.PatientPin = this.patientPin;
        this.patientService.GetPatientInfoList(this.patientPin, this.patientSerachParamsValue).subscribe(data => {
          this.patientInfoList = data;
          if (this.patientInfoList[0].DateOfBirth) {
            this.patientInfoList[0].DateOfBirth = new Date(this.patientInfoList[0].DateOfBirth);
          }
          this.patientBannerConfiguration.IsHideInsurance = true;
          this.patientVisitLogParams = {
            pin: this.patientInfoList[0].PatientPin,
            ClinicId: this.clinicId,
            usr_id: this.LogInUserId
          };
        });
      } else {
        this.patientInfoList = undefined;
        if (this.patientBannerConfiguration) {
          this.patientBannerConfiguration.IsHideInsurance = false;
        }
        this.patientOverdueRemittance = undefined;
      }
    }
  }

  // Closing the reschedule Appointment dialog box

  public rescheduleAppointmentClose() {
    this.showRecheduleDialog = false;
    this.clearAppointment();
  }

  // saving the  Rescheduled Data

  private async saveRescheduleData() {
    let validation: boolean = false;
    if (this.schedulerSetting.RemarksMandatory.Value1 === Yes && this.rescheduleAppointmentStatus === CVI) {
      if (!this.appointmentRemarks && this.appointmentRemarks.replace(/\s/g, '').length === 0) {
        this.validationMessage = 'CommonModule.EnterRemarks';
        validation = true;
      }
    }
    if (this.durationHour && this.durationMinute && this.durationHour !== '' && this.durationMinute !== '') {
      if (new Date(this.defaultDateHypen.transform(this.appointmentDateTime) + ' ' + this.shortTime.transform(this.appointmentDateTime)) < new Date()) {
        validation = true;
        this.validationMessage = 'AppointmentModule.PastTime';
      }
    } else {
      validation = true;
      this.validationMessage = 'AppointmentModule.EnterDuraion';
    }
    if (!validation) {
      const saveStatus = this.rescheduleAppointmentStatus ? this.rescheduleAppointmentStatus : this.beforeRescheduleStatus;
      if (this.previousAppointmentDate !== this.appointmentDateTime && saveStatus !== CVI) {
        this.isSendSms = true;
      }
      const appointmentParam = {
        appointmentId: this.appointmentId,
        userId: this.LogInUserId,
        doctorId: this.doctorUserId,
        specialityId: this.specialityId,
        duration: this.durationHour + ':' + this.durationMinute,
        appointmentDate: this.defaultDateHypen.transform(this.appointmentDateTime),
        remarks: this.appointmentRemarks ? this.appointmentRemarks : '',
        time: this.shortTime.transform(this.appointmentDateTime),
        status: this.rescheduleAppointmentStatus ? this.rescheduleAppointmentStatus : this.beforeRescheduleStatus,
        clinicId: this.clinicId,
        departmentId: this.departmentId !== 0 ? this.departmentId : this.getResourceDetails(this.doctorUserId).DepartmentId,
        isSendSms: this.isSendSms
      };
      this.isSendSms = false;
      let messageStr: string = '';
      this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentSaved',
        // tslint:disable-next-line: max-line-length
        { PatientName: this.patientName, DoctorName: this.selectedResource, Date: this.defaultDate.transform(this.appointmentDateTime), Time: this.shortTime.transform(this.appointmentDateTime) }).then(translatedMessage => {
          messageStr = translatedMessage;
        });
      if (this.showProcedureList) {
        if (this.appointmentProcedureList.length > 0) {
          for (let procedureIndex = 0; procedureIndex < this.appointmentProcedureList.length; procedureIndex++) {
            this.appointmentProcedureList[procedureIndex].APM_ID = this.appointmentId;
            this.appointmentProcedureList[procedureIndex].ATD_SEQ = procedureIndex + 1;
            this.appointmentProcedureList[procedureIndex].ATR_CREATEDBY = this.LogInUserId;
          }
          this.schedulerService.SaveAppointmentProcedure(this.appointmentProcedureList).subscribe(procedureData => {
            this.updateAppointment(appointmentParam);
            this.getAppointment();
            this.showRecheduleDialog = false;
          });
        } else if (this.deleteappointmentProcedureList.length > 0) {
          this.schedulerService.DeleteAppointmentProcedure(this.appointmentId).subscribe(procedureData => {
            this.updateAppointment(appointmentParam);
            this.getAppointment();
            this.showRecheduleDialog = false;
          });
        } else {
          this.updateAppointment(appointmentParam);
          this.getAppointment();
          this.showRecheduleDialog = false;
        }
      } else {
        await this.updateAppointment(appointmentParam);
        this.getAppointment();
        this.showRecheduleDialog = false;
      }
    }
  }

   // saving  Rescheduled Appointment

  public saveRescheduleAppointment() {
    this.validationMessage = '';
    this.rescheduleSaveButton = false;
    if (this.defaultDate.transform(this.appointmentDateTime) === this.defaultDate.transform(this.selectedDate)) {
      let appointmentEnd: Date = new Date(this.appointmentDateTime.getTime() + (Number(this.durationHour) * 60 * 60 * 1000));
      appointmentEnd = new Date(appointmentEnd.getTime() + Number(this.durationMinute) * 60000);
      if (this.onCheckSlotAvailability(this.appointmentDateTime, appointmentEnd, this.scheduler.events, this.doctorUserId, this.appointmentId)) {
        if (this.checkWorkingHour(this.doctorUserId, this.appointmentDateTime, appointmentEnd)) {
          if (!this.onCheckAppointmentAvailability(this.appointmentId, this.appointmentDateTime, appointmentEnd, this.appointmentData, this.doctorUserId, this.patientPin, this.patientMobileNumber)) {
            this.saveRescheduleData();
            this.rescheduleSaveButton = true;
          } else {
            this.rescheduleSaveButton = true;
            this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentAvailability',
              // tslint:disable-next-line: max-line-length
              { PatientName: this.patientName, DoctorName: this.getResourceDetails(this.doctorUserId).DoctorName, Date: this.defaultDate.transform(this.appointmentDateTime), ShortTime: this.shortTime.transform(this.appointmentDateTime) }).then(translatedMessage => {
                this.validationMessage = translatedMessage;
              });
          }
        } else {
          this.rescheduleSaveButton = true;
          this.validationMessage = 'AppointmentModule.AppointmentSlotCheck';
        }
      } else {
        this.rescheduleSaveButton = true;
        this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentSlotValidation',
          // tslint:disable-next-line: max-line-length
          { Date: this.defaultDate.transform(this.appointmentDateTime), ShortTime: this.shortTime.transform(this.appointmentDateTime), DoctorName: this.getResourceDetails(this.doctorUserId).DoctorName, Speciality: this.getResourceDetails(this.doctorUserId).DoctorSpeciality }).then(translatedMessage => {
            this.validationMessage = translatedMessage;
          });
      }
    } else {
      const appointmnentParam = {
        doctorId: this.doctorUserId,
        clinicId: this.clinicId,
        duration: this.durationHour + ':' + this.durationMinute,
        appointmentDate: new DatePipe('en-US').transform(this.appointmentDateTime, 'yyyy-MM-dd h:mm:ss a'),
        appointmentTime: new DatePipe('en-US').transform(this.appointmentDateTime, 'yyyy-MM-dd h:mm:ss a')
      };
      this.schedulerService.GetEventVerification(appointmnentParam).subscribe(data => {
        if (data.Message === 'OK') {
          this.saveRescheduleData();
          this.rescheduleSaveButton = true;
        } else {
          this.rescheduleSaveButton = true;
          this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentSlotValidation',
            // tslint:disable-next-line: max-line-length
            { Date: this.defaultDate.transform(this.appointmentDateTime), ShortTime: this.shortTime.transform(this.appointmentDateTime), DoctorName: this.getResourceDetails(this.doctorUserId).DoctorName, Speciality: this.getResourceDetails(this.doctorUserId).DoctorSpeciality }).then(translatedMessage => {
              this.validationMessage = translatedMessage;
            });
        }
      });
    }
  }

  // change in doctor on the booked appointment

  public onDoctorChange(event: any): void {
    this.doctorCount = 1;
    if (event) {
      this.doctorId = 0;
      this.doctorUserId = 0;
      this.multiSelectDoctorValue = event;
    } else {
      this.multiSelectDoctorValue = [];
      this.doctorId = 0;
      this.doctorUserId = 0;
    }
    if (!this.isOnLoad && (!this.IsDoctorLogin || this.userCanScheduleUser)) {
      this.applyFilterForAppointment();
    }
  }

  // change in doctor on the rescheduled appointment

  public onRescheduleDoctorChange(event: any): void {
    if (event) {
      this.doctorId = event.DoctorId;
      this.doctorUserId = event.UserId;
    }
  }

  // scrolling the appointment page to the current schedule

  public scrollToCurrentTime(): void {
    // if (this.selectedView === 'day') {
    setTimeout(() => {
      const el = document.querySelector('.k-current-time:not(.k-current-time-arrow-right)');
      if (el) {
        el.scrollIntoView({ block: 'center', inline: 'center' });
        this.enableCurrentTime = true;
      } else {
        this.enableCurrentTime = false;
      }
    });
    // }
  }

  // expanding the appointment page

  public eventResizeEndHandler(event: any): void {
    this.showAppointmentInfo = false;
    // tslint:disable-next-line: max-line-length
    if (event.event.dataItem.dataItem.StatusShort !== CVI && event.event.dataItem.dataItem.StatusShort !== APH && event.event.dataItem.dataItem.StatusShort !== NSW && this.checkWorkingHour(event.dataItem.appointmentUserId, event.start, event.end)) {
      // tslint:disable-next-line: max-line-length
      if (this.onCheckSlotAvailability(event.start, event.end, event.sender.events, this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.dataItem.appointmentUserId, event.event.dataItem.dataItem.AppointmentId)) {
        // tslint:disable-next-line: max-line-length
        if (!this.onCheckAppointmentAvailability(event.event.dataItem.dataItem.AppointmentId, event.start, event.end, this.appointmentData, this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.dataItem.appointmentUserId, event.event.dataItem.dataItem.PatientPin, event.event.dataItem.dataItem.PatientMobile)) {
          this.clearAppointment();
            this.isSendSms = false;
          const appointmentParam = {
            appointmentId: event.event.dataItem.dataItem.AppointmentId,
            userId: this.LogInUserId,
            doctorId: this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.dataItem.appointmentUserId,
            specialityId: this.IsDoctorLogin && !this.userCanScheduleUser ? this.specialityId : this.getResourceDetails(event.event.dataItem.dataItem.AppointmentUserId).SpecialityID,
            duration: this.durationCalculation(event.start, event.end),
            appointmentDate: this.defaultDateHypen.transform(new Date(event.event.dataItem.dataItem.AppointmentTime)),
            remarks: event.event.dataItem.dataItem.Remarks ? event.event.dataItem.dataItem.Remarks : '',
            time: this.shortTime.transform(new Date(event.event.dataItem.dataItem.AppointmentTime)),
            status: event.event.dataItem.dataItem.StatusShort,
            clinicId: this.IsDoctorLogin && !this.userCanScheduleUser ? event.dataItem.clinicId : this.clinicId,
            departmentId: this.departmentId !== 0 ? this.departmentId : this.getResourceDetails(event.event.dataItem.dataItem.AppointmentUserId).DepartmentId,
            isSendSms: this.isSendSms
          };
          this.isSendSms = false;
          if (event.end >= new Date()) {
            this.updateAppointment(appointmentParam);
          } else {
            this.clearAppointment();
          }
        } else {
          this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentAvailability',
            // tslint:disable-next-line: max-line-length
            { PatientName: event.event.dataItem.dataItem.PatientFirstName, DoctorName: this.getResourceDetails(this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.dataItem.appointmentUserId).DoctorName, Date: this.defaultDate.transform(event.start), ShortTime: this.shortTime.transform(event.start) }).then(translatedMessage => {
              this.toastr.warningToastr(translatedMessage, this.generalService.onTranslateService('CommonModule.Validation'), {
                toastTimeout: 3000
              });
            });
        }
      } else {
        this.generalService.getMultiTranslatedMessage('AppointmentModule.AppointmentSlotValidation',
          // tslint:disable-next-line: max-line-length
          { Date: this.defaultDate.transform(event.start), ShortTime: this.shortTime.transform(event.start), DoctorName: this.getResourceDetails(this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.dataItem.appointmentUserId).DoctorName, Speciality: this.getResourceDetails(this.IsDoctorLogin && !this.userCanScheduleUser ? this.doctorUserId : event.dataItem.appointmentUserId).DoctorSpeciality }).then(translatedMessage => {
            this.toastr.warningToastr(translatedMessage, this.generalService.onTranslateService('CommonModule.Validation'), {
              toastTimeout: 3000
            });
          });
      }
    }
  }

  // updating or modifying the booked Appointment

  private updateAppointment(appointmentParam: any): void {
    this.schedulerService.RescheduleAppointment(appointmentParam).subscribe(data => {
      this.clearAppointment();
      this.getAppointment();
    });
  }

  // calculating the duration of the booked appointments

  private durationCalculation(startDate: any, endDate: any): string {
    const diff = endDate - startDate;
    const diffHrs = Math.floor((diff % 86400000) / 3600000);
    const diffMins = Math.round(((diff % 86400000) % 3600000) / 60000);
    return diffHrs.toString() + ':' + diffMins.toString();
  }

  // display or listing the calculated Duration

  private displayDurationCaluculation(startDate: any, endDate: any): void {
    const diff = endDate - startDate;
    const diffHrs = Math.floor((diff % 86400000) / 3600000);
    const diffMins = Math.round(((diff % 86400000) % 3600000) / 60000);
    this.durationHour = diffHrs.toString();
    this.durationMinute = diffMins.toString();
  }

  // All the Resource Details are listed using this function

  private getResourceDetails(selectedDoctorId: any): any {
    for (let doctorIndex = 0; doctorIndex < this.doctorList.length; doctorIndex++) {
      if (this.doctorList[doctorIndex].DoctorId === selectedDoctorId) {
        return this.doctorList[doctorIndex];
        break;
      }
    }
  }

  // All the buttin Click actions

  public eventClick(event: any, data: any): void {
    this.anchor = event.currentTarget;
    this.selectedEvent = data;
    if (!this.moveToQueueClick && !this.moreButtonClick && !this.dragClick) {
      this.clearAppointment();
      this.showAppointmentInfo = true;
      this.isFirstTimeClick = true;
      this.appointmentStartDate = new Date(data.dataItem.dataItem.AppointmentTime);
      this.appointmentDateTime = new Date(data.dataItem.dataItem.AppointmentTime);
      this.appointmenEndTime = new Date(data.dataItem.dataItem.AppointmentTimeEnd);
      this.patientPin = data.dataItem.dataItem.PatientPin;
      this.patientMobileNumber = data.dataItem.dataItem.PatientMobile;
      this.patientName = data.dataItem.dataItem.PatientFirstName;
      this.rescheduleAppointmentStatus = data.dataItem.dataItem.Status;
      this.beforeRescheduleStatus = data.dataItem.dataItem.StatusShort;
      this.appointmentRemarks = data.dataItem.dataItem.Remarks;
      this.patientAppointmentType = data.dataItem.dataItem.AppointmentType;
      this.isInsurance = data.dataItem.dataItem.IsInsurance;
      this.appointmentCreatedBy = data.dataItem.dataItem.CreatedBy;
      this.patientDateOfBirth = new Date(data.dataItem.dataItem.PatientDateOfBirth);
      this.patientGender = data.dataItem.dataItem.PatientGender;
      this.appointmentStatusColor = data.dataItem.dataItem.StatusColor;
      this.appointmentStatusList = this.filterAppointmentSelectedStatus(data.dataItem.dataItem.StatusShort, true);
      this.appointmentId = data.dataItem.dataItem.AppointmentId;
    } else {
      this.moveToQueueClick = false;
      this.moreButtonClick = false;
      this.dragClick = false;
    }
  }

  // closing the dialog box of the appointment Info

  public appointmentInfoClose(): void {
    this.showAppointmentInfo = false;
    this.clearAppointment();
  }

  // Dialog info of success,warning or failure

  private showCommonAlertDialog(title: string, translatedMessage: string, buttonValues: any, message: string): void {
    this.showAlertDialog = true;
    this.alertDialogData.title = title;
    this.alertDialogData.translatedMessage = translatedMessage;
    this.alertDialogData.buttonValues = buttonValues;
    this.alertDialogData.message = message;
  }

  // show Common Alerts on success,warning or failure

  private showCommonAlert(alertTitle: string, alertMessage: string, alertTranslatedMessage: string, messageType: string): void {
    this.showAlert = true;
    this.alertMsg = {
      title: alertTitle,
      message: alertMessage,
      translatedMessage: alertTranslatedMessage,
      messageType: messageType
    };
  }

  // Closing the dialog boxes

  public async onAlertDialogClose(close) {
    if (close.alertId !== undefined) {
      if (close.alertId.toUpperCase() === NewPatientAlert) {
        if (close.buttonId.toUpperCase() === Yes) {
          this.redirectToPatientRegister();
        } else {
          this.clearAppointment();
        }
      } else if (close.alertId.toUpperCase() === PatientMigrationAlert) {
        if (close.buttonId.toUpperCase() === Yes) {
          this.redirectToPatientRegister();
        } else {
          this.clearAppointment();
        }
      } else if (close.alertId.toUpperCase() === MoveToQueue) {
        if (close.buttonId.toUpperCase() === Yes) {
          this.addPatientToQueue();
        } else {
          this.clearAppointment();
        }
      }
      this.showAlertDialog = false;
    }
  }

  // Closing te alert boxes

  public onAlertClose(event: any): void {
    this.showAlert = event.dismiss;
  }

  // redirection  To Patient Register

  private async redirectToPatientRegister() {
    const queryParams: IvisitqueuePatientRegisterParams = {
      callFrom: Scheduler,
      patientPin: this.patientPin,
      patientName: this.patientName,
      mobileNumber: this.patientMobileNumber,
      doctorId: this.doctorId,
      departmentId: this.departmentId !== 0 ? this.departmentId : this.getResourceDetails(this.doctorUserId).DepartmentId,
      specialityId: this.specialityId,
      AppointmentDate: this.appointmentDateTime,
      doctorUserId: this.doctorUserId,
      appointmentId: this.appointmentId,
      returnURL: this.router.url
    };
    await this.patientRegisterCall(queryParams);
  }

  // Trigger the patient Register page

  private async patientRegisterCall(queryParams: any) {
    this.clearAppointment();
    const navigationExtras: NavigationExtras = { queryParams };
    const urlPath = await this.menuMasterService.getUrlPath(MenuMasterService.patientRegisterPage);
    this.router.navigate([urlPath], navigationExtras);
  }

  // Listing the patients on queue as per the slot

  private addPatientToQueue(): void {
    const appointmentParam = {
      appointmentId: this.appointmentId,
      userId: this.LogInUserId
    };
    const visitParam: ICheckPatientVisit = {
      patientPin: this.patientPin,
      doctorId: this.doctorId,
      departmentId: this.departmentId !== 0 ? this.departmentId : this.getResourceDetails(this.doctorUserId).DepartmentId,
      userId: this.doctorUserId,
      clinicId: this.currentClinicId,
      visitCheckInDatetime: this.defaultDateHypen.transform(this.appointmentDateTime),
      loginUserId: this.LogInUserId
    };
    this.schedulerService.CheckVisitAlreadyAvailable(visitParam).subscribe(checkData => {
      if (checkData.Message === False) {
        this.schedulerService.MoveToQueue(appointmentParam).subscribe(data => {
          this.getAppointment();
          this.clearAppointment();
        });
      } else {
        this.generalService.getMultiTranslatedMessage('CommonComponentModule.AlreadyAddedInQueue',
          { patientName: this.patientName }).then(translatedMessage => {
            this.showCommonAlert('CommonModule.Validation', '', translatedMessage, MessageTypeenum.WARNING);
          });
      }
    });
  }

  // More patient information will be listed using this function

  public showUserMoreInfo(): void {
    this.showUserInfo = this.showUserInfo ? false : true;
    if (this.showUserInfo) {
      this.showCamera = NoCameraIcon;
      this.showCameraText = 'CommonModule.HideDoctorDetails';

    } else {
      this.showCamera = CameraIcon;
      this.showCameraText = 'CommonModule.ShowDoctorDetails';
    }
  }

  // Status level data change based on the selection of values

  public onSelectStatus(event: any): void {
    this.rescheduleAppointmentStatus = event;
    // this.appointmentId = data.dataItem.dataItem.AppointmentId;
    if (event === CancelAppointment) {
      this.enableRemarksPopUp = true;
      this.remarksInput.IsRemarksMandatory = this.schedulerSetting.RemarksMandatory.Value1 === Yes ? true : false;
      this.remarksInput.Remarks = this.appointmentRemarks ? this.appointmentRemarks : '';
      this.remarksInput.RemarksCode = '';
      this.remarksInput.RemarksType = 'AppointmentModule.AppointmentCancellationRemarks';
      this.remarksInput.RemarksMaxLength = 1024;
      this.remarksInput.RemarksTitle = 'AppointmentModule.EnterCancellationRemarks';
    } else {
      this.updateStatus();
    }
  }

  // updating the Status from visited,waiting,cancelled

  public updateStatus(): void {
    const appointmentParam = {
      appointmentId: this.appointmentId,
      status: this.rescheduleAppointmentStatus,
      userId: this.LogInUserId,
      remarks: this.appointmentRemarks ? this.appointmentRemarks : '',
      clinicId: this.clinicId,
      departmentId: this.departmentId !== 0 ? this.departmentId : this.getResourceDetails(this.doctorUserId).DepartmentId
    };
    this.schedulerService.UpdateAppointmentStatus(appointmentParam).subscribe(data => {
      this.getAppointment();
      this.clearAppointment();
    });
  }

  // remarks dialog page Closing control

  public remarksOutputClose(event) {
    this.enableRemarksPopUp = event.dismiss;
    if (event.remarks !== undefined) {
      this.appointmentRemarks = event.remarks;
      this.updateStatus();
    } else {
      this.appointmentRemarks = '';
      this.clearAppointment();
    }
  }

  // Appointment Setting as per the requirement of the clinic

  public showAppointmentSetting(): void {
    this.appointmentSettingErrorText = '';
    this.showSettingPopup = true;
  }

  // closing the dialog boxed of the Appointment

  public closeAppointmentSettingDialog(): void {
    this.noOfResourcePerPage = this.schedulerSetting.DoctorCount.Number1;
    this.subIntervalsSetting = this.schedulerSetting.ShrinkTimeSlot.Value2;
    this.shrinkDisplaytext = this.schedulerSetting.SlotDisplayValues.Value2;
    this.genderEnable = this.schedulerSetting.DisplayText.Value2 === Yes ? true : false;
    this.showSettingPopup = false;
  }

  // saving the  Appointment Setting

  public saveAppointmentSetting(): void {
    this.appointmentSettingErrorText = '';
    if (this.subIntervalsSetting && this.subIntervalsSetting > 0 && this.subIntervalsSetting < 5) {
      if (this.noOfResourcePerPage && this.noOfResourcePerPage > 0 && this.noOfResourcePerPage < 6) {
        const labelSettings: ICodeMaster[] = [];
        this.schedulerSetting.DoctorCount.Number1 = this.noOfResourcePerPage;
        labelSettings.push(this.schedulerSetting.DoctorCount);
        this.schedulerSetting.ShrinkTimeSlot.Value2 = this.subIntervalsSetting;
        this.schedulerSetting.ShrinkTimeSlot.Value1 = 60 / this.subIntervalsSetting;
        labelSettings.push(this.schedulerSetting.ShrinkTimeSlot);
        this.schedulerSetting.SlotDisplayValues.Value2 = this.shrinkDisplaytext;
        labelSettings.push(this.schedulerSetting.SlotDisplayValues);
        this.schedulerSetting.DisplayText.Value2 = this.genderEnable ? Yes : No;
        labelSettings.push(this.schedulerSetting.DisplayText);
        this.codeMasterService.SaveCodeMasterUserSetting(labelSettings, this.LogInUserId, this.currentClinicId, 0).subscribe(response => {
          if (response.Response && response.Response.toUpperCase() === Sucess) {
            this.fetchCodeMasterConfiguration();
            this.showSettingPopup = false;
            this.toastr.successToastr(this.generalService.onTranslateService('AppointmentModule.AppointmentSettingSaved'), this.generalService.onTranslateService('CommonModule.Success'), {
              toastTimeout: 3000
            });
          } else {

          }
          this.blockUIComponent.stopBlockUI();
        });
      } else {
        this.appointmentSettingErrorText = 'AppointmentModule.EnterNoOfResource';
      }
    } else {
      this.appointmentSettingErrorText = 'AppointmentModule.EnterSubInterval';
    }
  }

  // scheduler page navigation & exploring

  public schedulerViewChange(event: any): void {
    this.showSettingPopup = true;
  }

  // closing the Appointment Report page dialogs

  public closeAppointmentReport(event: any): void {
    this.openAppointmentReport = event.dismiss;
  }
   // Listing & showing the Appointment Reports in a dialog
   public showAppointmentReport(): void {
    this.openAppointmentReport = true;
    }

  // Closing the More Action dialog page
  public onCloseMoreActionClick(): void {
    this.appointmentId = 0;
    this.showMoreOption = false;
    }

  // drop down level change event on the appointment filter type
  public onEventCreationAppointmentTypeFilterChange(value: any): void {
    this.eventAppointmentTypeList = this.schedulerSetting.AppointmentType.filter((s) => s.Value1.toLowerCase().indexOf(value.toLowerCase()) !== -1);
    }

  // Showing or listing Slots
  private onCheckSlotAvailability(startDate: Date, endDate: Date, eventList: any, userId: any, checkAppointmentId: any): boolean {
    let count = 0;
    let isSlotAvailable = true;
    if (this.defaultDate.transform(startDate) === this.defaultDate.transform(this.selectedDate)) {
      for (let eventIndex = 0; eventIndex < eventList.length; eventIndex++) {
        // tslint:disable-next-line:max-line-length
        if (eventList[eventIndex].id !== checkAppointmentId && eventList[eventIndex].appointmentUserId && eventList[eventIndex].dataItem.StatusShort !== CVI && eventList[eventIndex].appointmentUserId === userId && startDate >= new Date(eventList[eventIndex].start) && startDate <= new Date(eventList[eventIndex].end)) {
          count++;
        }
      }
      if (count >= this.schedulerSetting.TimeSlot.Number1) {
        isSlotAvailable = false;
      }
    }
    return isSlotAvailable;
    }

  // Back button functionality on the scheduler page
  public previousButtonClick(): void {
    this.doctorCount--;
    this.applyFilterForAppointment();
    }

  // next button functionality on the scheduler page
  public nextButtonClick(): void {
    this.doctorCount++;
    this.applyFilterForAppointment();
    }

  // Appointment procedure list
  public onProcedureChange(event: any): void {
    this.appointmentProcedureList = event;
    }

  // Searching the Procedures on the appointment page
  public handleProcedureSearch(value): void {
    this.loadingProcedure = true;
    if (this.webservicecall) {
      this.webservicecall.unsubscribe();
    }
    this.procedureList = [];
    const param = {
      userId: this.LogInUserId,
      clinicId: this.currentClinicId,
      searchString: value
    };
    this.webservicecall = this.schedulerService.GetProcedure(param).subscribe(procedureData => {
      this.procedureList = procedureData;
      this.sourceProcedureList = procedureData;
      this.loadingProcedure = false;
      if (this.procedureList.length <= 0) {
        this.searchStatus = 'CommonModule.NoDataFound';
      }
    });
    }

  // filter level change events on the Procedures
  public procedureFilterChange(event: any): void {
    this.searchStatus = 'LoaderModule.Default';
    this.subject.next(event);
    }

  // Deleting the Procedures
  public onRemoveProcedure(event: any): void {
    for (let procedureIndex = 0; procedureIndex < this.appointmentProcedureList.length; procedureIndex++) {
      if (this.appointmentProcedureList[procedureIndex].INT_CODE === event.dataItem.INT_CODE) {
        this.deleteappointmentProcedureList.push(this.appointmentProcedureList[procedureIndex]);
        this.appointmentProcedureList.splice(procedureIndex, 1);
        break;
      }
    }
    }

  // restricting the input fields to enter Only Numbers
  public restrictOnlyNumbers(event: any) {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57 || event.keyCode === 9 || event.keyCode === 37 || event.keyCode === 39)) {
      return false;
    }
    return true;
    }

  // restricting the input fields to enter Only alphabets
  public restrictOnlyAlphabets(event: any) {
    // tslint:disable-next-line:max-line-length
    if ((event.keyCode > 64 && event.keyCode < 91) || (event.keyCode > 96 && event.keyCode < 123) || event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 37 || event.keyCode === 39 || event.keyCode === 32 || event.keyCode === 222) {
      return true;
    }    else {
      return false;
    }
    }

  // Closing the popups
  public popupclose(): void {
    this.showMoreOption = false;
    this.showAppointmentInfo = false;
    }

  // Listing the Working Hours
  private checkWorkingHour(userId: any, startDate: any, endDate: any): boolean {
    const dateString = new DatePipe('en-US').transform(startDate, 'MM/dd/yyyy');
    const doctorWorkingHours = this.doctorWorkingHour.filter((s) => s.DoctorId === userId && s.Start.indexOf(dateString) !== -1);
    if (!doctorWorkingHours[0]) {
      return false;
    } else {
      if (startDate >= new Date(doctorWorkingHours[0].Start) && startDate < new Date(doctorWorkingHours[0].End) && endDate <= new Date(doctorWorkingHours[0].End)) {
        return true;
      }      else {
        return false;
      }
    }
    }

  // Checking the Availibility of the appointment slots
  private onCheckAppointmentAvailability(appointmentId: any, startDate: Date, endDate: Date, eventList: any, userId: any, patientPin: any, mobileNumber: any): boolean {
    let isAppointmentAvailable = false;
    if (this.defaultDate.transform(startDate) === this.defaultDate.transform(this.selectedDate)) {
      for (let eventIndex = 0; eventIndex < eventList.length; eventIndex++) {
        if (patientPin !== 0) {
          if (appointmentId !== 0) {
            // tslint:disable-next-line:max-line-length
            if (eventList[eventIndex].AppointmentId !== appointmentId && eventList[eventIndex].StatusShort !== CVI && eventList[eventIndex].PatientPin === patientPin && (eventList[eventIndex].AppointmentUserId === userId && ((startDate >= new Date(eventList[eventIndex].AppointmentTime) && endDate <= new Date(eventList[eventIndex].AppointmentTimeEnd)) || (startDate < new Date(eventList[eventIndex].AppointmentTimeEnd) && endDate >= new Date(eventList[eventIndex].AppointmentTimeEnd)) || (startDate <= new Date(eventList[eventIndex].AppointmentTime) && endDate >= new Date(eventList[eventIndex].AppointmentTime))))) {
              isAppointmentAvailable = true;
              break;
            }
          } else {
            // tslint:disable-next-line:max-line-length
            if (eventList[eventIndex].PatientPin === patientPin && eventList[eventIndex].StatusShort !== CVI && (eventList[eventIndex].AppointmentUserId === userId && ((startDate >= new Date(eventList[eventIndex].AppointmentTime) && endDate <= new Date(eventList[eventIndex].AppointmentTimeEnd)) || (startDate < new Date(eventList[eventIndex].AppointmentTimeEnd) && endDate >= new Date(eventList[eventIndex].AppointmentTimeEnd)) || (startDate <= new Date(eventList[eventIndex].AppointmentTime) && endDate >= new Date(eventList[eventIndex].AppointmentTime))))) {
              isAppointmentAvailable = true;
              break;
            }
          }
        } else {
          if (appointmentId !== 0) {
            // tslint:disable-next-line:max-line-length
            if (eventList[eventIndex].AppointmentId !== appointmentId && eventList[eventIndex].StatusShort !== CVI && eventList[eventIndex].PatientMobile === mobileNumber && (eventList[eventIndex].AppointmentUserId === userId && ((startDate >= new Date(eventList[eventIndex].AppointmentTime) && endDate <= new Date(eventList[eventIndex].AppointmentTimeEnd)) || (startDate < new Date(eventList[eventIndex].AppointmentTimeEnd) && endDate >= new Date(eventList[eventIndex].AppointmentTimeEnd)) || (startDate <= new Date(eventList[eventIndex].AppointmentTime) && endDate >= new Date(eventList[eventIndex].AppointmentTime))))) {
              isAppointmentAvailable = true;
              break;
            }
          } else {
            // tslint:disable-next-line:max-line-length
            if (eventList[eventIndex].PatientMobile === mobileNumber && eventList[eventIndex].StatusShort !== CVI && (eventList[eventIndex].AppointmentUserId === userId && ((startDate >= new Date(eventList[eventIndex].AppointmentTime) && endDate <= new Date(eventList[eventIndex].AppointmentTimeEnd)) || (startDate < new Date(eventList[eventIndex].AppointmentTimeEnd) && endDate >= new Date(eventList[eventIndex].AppointmentTimeEnd)) || (startDate <= new Date(eventList[eventIndex].AppointmentTime) && endDate >= new Date(eventList[eventIndex].AppointmentTime))))) {
              isAppointmentAvailable = true;
              break;
            }
          }
        }
      }
    }
    return isAppointmentAvailable;
  }
}
